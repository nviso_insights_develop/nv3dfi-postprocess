# README

## How to generate the html report for all videos

### One annotation file per video results

    ./script/generate_reports.sh classic_light_full_70x60_float_fps_2 all {ID}

### Common annotation file for all video results

    ./script/generate_reports.sh classic_light_full_70x60_float_fps_2 common annotation/{ID}/annotation.json


## How to generate a single html report

1) Generate results for html report (gen\_results.py)
    - For more information read the python usefull commands

2) Open with Firefox the file report/index.html


## Python usefull commands

### Apply filters

Parse the video processing results and apply filters.

    ./python/gen_results.py -i BRPR2012_sdk_2.json -c config/export_config.json \
                            --initial-bias-duration -1 \
                            --export-config config/export_config.json \
                            -o report


### Compare results

Generate json results for html report.

    ./python/gen_results.py -i BRPR2012_sdk_2.json \
                            --export-config config/export_config.json \
                            --expected annotation/{ID}/annotation.json \
                            --compare-config config/compare_config.json \
                            --highlight speak \
                            --split \
                            -o report


### Show results

Compare our results with expected results, using a graph.

    ./python/show_results.py --up results.json --key-up confidence \
                             --down annotation/{ID}/annotation.json --key-down intensity \
                             --highlight speak


### Export images from a video

Export a part of a video, to images or to a gif.

Images:

    ./python/get_video_image.py -i ~/Downloads/BRPR2012.avi -t 100 --step 1000

Gif:

    ./python/get_video_image.py -i ~/Downloads/BRPR2012.avi -t 100 --margin 1000 -o name.gif

