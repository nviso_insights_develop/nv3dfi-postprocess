set -e
set -x

# Convert all the PSA CSV annotation to JSON

./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ BRPR2012_Expert.csv -o annotation/psa/BRPR2012_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ DAVI1912_Expert.csv -o annotation/psa/DAVI1912_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ EMLA1312_Expert.csv -o annotation/psa/EMLA1312_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ FABR2012_Expert.csv -o annotation/psa/FABR2012_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ FLDE0612_Expert.csv -o annotation/psa/FLDE0612_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ HABO2012_Expert.csv -o annotation/psa/HABO2012_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ JEDE0712_Expert.csv -o annotation/psa/JEDE0712_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ JEFA0712_Expert.csv -o annotation/psa/JEFA0712_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ JELE1512_Expert.csv -o annotation/psa/JELE1512_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ JEWA1912_Expert.csv -o annotation/psa/JEWA1912_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ JUMA0612_Expert.csv -o annotation/psa/JUMA0612_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ KOBE0512_Expert.csv -o annotation/psa/KOBE0512_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ LABO0912_Expert.csv -o annotation/psa/LABO0912_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ LOFR2012_Expert.csv -o annotation/psa/LOFR2012_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ MAKA0912_Expert.csv -o annotation/psa/MAKA0912_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ MATI0812_Expert.csv -o annotation/psa/MATI0812_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ MIAL0612_Expert.csv -o annotation/psa/MIAL0612_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ PHAN1912_Expert.csv -o annotation/psa/PHAN1912_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ PHGO0812_Expert.csv -o annotation/psa/PHGO0812_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ SAAH1512_Expert.csv -o annotation/psa/SAAH1512_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ SEVA1912_Expert.csv -o annotation/psa/SEVA1912_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ SIGA0812_Expert.csv -o annotation/psa/SIGA0812_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ SOKH1412_Expert.csv -o annotation/psa/SOKH1412_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ THBI0912_Expert.csv -o annotation/psa/THBI0912_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ THME1612_Expert.csv -o annotation/psa/THME1612_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ XAFR1612_Expert.csv -o annotation/psa/XAFR1612_ref.json
./python/psa/convert_csv_to_json.py -i annotation/psa/Participant\ XAGR1312_Expert.csv -o annotation/psa/XAGR1312_ref.json
