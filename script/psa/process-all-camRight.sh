set -x
set -e

# Process all the videos

# First  paramter: path th linux_host build folder
# Second paramter: path to video files base folders
# Third  paramter: path to output json files

# Usage ex: ./script/process-all-camRight.sh $HOME/devel/git/nv3dfi-build/install $HOME/net/synology

if [ $# -eq 0 ]
  then
    echo "\nError: The path to linux_host build folder is missing !\n"
    exit 1
fi

if [ $# -eq 1 ]
  then
    #Ex: $HOME/net/synology/CUSTOMER_DATASET/PSA/Full/PSA/recordings
    #Ex: HOME/net/synology
    echo "\nError: The path to videos is missing !\n"
    exit 2
fi

if [ $# -eq 2 ]
  then
    echo "\nError: The path output files is missing !\n"
    exit 3
fi

psa=`pwd`

recordings=$2

mkdir -p $3

cd $1

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$1/linux_host/lib

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/AHAA1412_20161214_092405/20161214_092602_LABCAR_508_PC3/LABCAR_508_PC3_20161214_092602_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/AHAA1412.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/BRPR2012_20161220_141308/20161220_141433_LABCAR_508_PC3/LABCAR_508_PC3_20161220_141433_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/BRPR2012.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/DAVI1912_20161219_144550/20161219_144733_LABCAR_508_PC3/LABCAR_508_PC3_20161219_144733_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/DAVI1912.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/EMLA1312_20161213_163417/20161213_163501_LABCAR_508_PC3/LABCAR_508_PC3_20161213_163501_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/EMLA1312.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/FABR2012_20161220_091535/20161220_091726_LABCAR_508_PC3/LABCAR_508_PC3_20161220_091726_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/FABR2012.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/FLDE0612_20161206_141500/20161206_141522_LABCAR_508_PC3/LABCAR_508_PC3_20161206_141522_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/FLDE0612.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/HABO2012_20161220_165314/20161220_165337_LABCAR_508_PC3/LABCAR_508_PC3_20161220_165337_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/HABO2012.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/JEDE0712_20161207_091240/20161207_091420_LABCAR_508_PC3/LABCAR_508_PC3_20161207_091420_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/JEDE0712.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/JELE1512_20161215_092348/20161215_092545_LABCAR_508_PC3/LABCAR_508_PC3_20161215_092545_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/JELE1512.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/JEWA1912_20161219_091348/20161219_091548_LABCAR_508_PC3/LABCAR_508_PC3_20161219_091548_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/JEWA1912.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/JUMA0612_20161206_092732/20161206_092814_LABCAR_508_PC3/LABCAR_508_PC3_20161206_092814_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/JUMA0612.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/KOBE0512_20161205_111708/20161205_111739_LABCAR_508_PC3/LABCAR_508_PC3_20161205_111739_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/KOBE0512.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/LABO0912_20161209_110136/20161209_110307_LABCAR_508_PC3/LABCAR_508_PC3_20161209_110307_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/LABO0912.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/LOFR2012_20161220_114156/20161220_114213_LABCAR_508_PC3/LABCAR_508_PC3_20161220_114213_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/LOFR2012.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/MAKA0912_20161209_161349/20161209_161606_LABCAR_508_PC3/LABCAR_508_PC3_20161209_161606_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/MAKA0912.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/MALE0512_20161205_140935/20161205_141157_LABCAR_508_PC3/LABCAR_508_PC3_20161205_141157_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/MALE0512.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/MATI0812_20161208_092451/20161208_092646_LABCAR_508_PC3/LABCAR_508_PC3_20161208_092646_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/MATI0812.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/MIAL0612_20161206_112042/20161206_112105_LABCAR_508_PC3/LABCAR_508_PC3_20161206_112105_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/MIAL0612.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/PHAN1912_20161219_105526/20161219_105544_LABCAR_508_PC3/LABCAR_508_PC3_20161219_105544_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/PHAN1912.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/PHGO0812_20161208_112356/20161208_112557_LABCAR_508_PC3/LABCAR_508_PC3_20161208_112557_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/PHGO0812.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/SAAH1512_20161215_164310/20161215_164450_LABCAR_508_PC3/LABCAR_508_PC3_20161215_164450_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/SAAH1512.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/SEVA1912_20161219_161505/20161219_161621_LABCAR_508_PC3/LABCAR_508_PC3_20161219_161621_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/SEVA1912.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/SIGA0812_20161208_141846/20161208_142120_LABCAR_508_PC3/LABCAR_508_PC3_20161208_142120_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/SIGA0812.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/SOKH1412_20161214_141112/20161214_141414_LABCAR_508_PC3/LABCAR_508_PC3_20161214_141414_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/SOKH1412.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/THBI0912_20161209_140508/20161209_140654_LABCAR_508_PC3/LABCAR_508_PC3_20161209_140654_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/THBI0912.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/THME1612_20161216_141623/20161216_141837_LABCAR_508_PC3/LABCAR_508_PC3_20161216_141837_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/THME1612.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/XAFR1612_20161216_113927/20161216_114005_LABCAR_508_PC3/LABCAR_508_PC3_20161216_114005_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/XAFR1612.json

time ./linux_host/bin/nv3dfi_cli nvisosdk/config/nv3dfi_config.json $psa/config/video.json $(nproc) -v \
        $recordings/XAGR1312_20161213_093031/20161213_093238_LABCAR_508_PC3/LABCAR_508_PC3_20161213_093238_camRight_imageOut.avi

sed 's/ //g' output.json > $psa/$3/XAGR1312.json

