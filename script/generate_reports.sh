#!/bin/bash

# Generate a report for all the video processing results

set -e

# First parameter: folder containing the video processiong json results
# Second parameter: "empty" to use empty annotation, "common" for common annotation or one annotation per result
# The third parameter: is the client id or annotation common file

if [[ "$1" == "" ]] ; then
    echo "Error: The first parameter, path to json results, is missing !"
    exit 1
fi

input_folder=$1
input_folder_basename=$(basename $input_folder)
output_folder=output_$input_folder_basename

cd $input_folder

filenames=$(find *)

bias_duration=0

cd -

set -x

rm -rf $output_folder
mkdir -p $output_folder

# Main index.html
cp report/list_results.html $output_folder/index.html

for filename in $filenames
do
    id="${filename/\.json/}"
    id_ref=$id"_ref"

    echo $id
    echo $id_ref

    if [ "$2" == "empty" ] ; then
        annotation=annotation/empty_with_bias.json
        #annotation=annotation/empty.json
    elif [ "$2" == "common" ] ; then
        annotation=$3
    else
        annotation=annotation/$3/$id_ref".json"
    fi

    echo "annotation: $annotation"

    if [ -f $annotation ]; then

        rm -f report/*.json
        rm -f report/*.js
        rm -f report/*.pdf

        ./python/gen_results.py -i $input_folder/$filename \
                                --export-config config/export_config.json \
                                --expected $annotation \
                                --compare-config config/compare_config.json \
                                --mode window \
                                --initial-bias-duration $bias_duration \
                                --highlight black_screen \
                                -o report \
                                --split

        cp -rf report $output_folder/$id
        rm -rf $output_folder/$id/fonts
        rm -rf $output_folder/$id/img
        cp -rf $output_folder/$id/nviso/css/* $output_folder/$id/css
        cp -rf $output_folder/$id/nviso/fonts $output_folder/$id
        cp -rf $output_folder/$id/nviso/img $output_folder/$id
        rm -rf $output_folder/$id/nviso
        rm $output_folder/$id/list_results.html

        # Export the image to show results of applied filters
        ./python/gen_results.py -i $input_folder/$filename \
                                --initial-bias-duration $bias_duration \
                                --expected $annotation \
                                --highlight black_screen \
                                --export-config config/export_config.json \
                                --export-graph $output_folder/$id/filters_results.png

        # Export the image to show results compare to annotations
        ./python/gen_results.py -i $input_folder/$filename \
                                --export-config config/export_config.json \
                                --expected $annotation \
                                --compare-config config/compare_config.json \
                                --mode window \
                                --initial-bias-duration $bias_duration \
                                -o /tmp
        ./python/show_results.py --up /tmp/results_sdk.json --key-up confidence \
                                 --down $annotation --key-down intensity \
                                 --highlight black_screen \
                                 --output $output_folder/$id/compare_results.png
    fi

    echo "========================================================================================="
done

./python/merge_results.py -i $output_folder

cp -rf report/js $output_folder
cp -rf report/css $output_folder
cp -rf report/nviso/css/* $output_folder/css
cp -rf report/nviso/fonts $output_folder
cp -rf report/nviso/img $output_folder

rm report/*.json
rm report/*.js

echo "Report:"
echo "file://`pwd`/$output_folder/index.html"
