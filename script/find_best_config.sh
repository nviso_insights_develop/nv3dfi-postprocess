set -e
set -x

# This script helps to find the best config for a given filter

# The first parameter is the path to the video processing results
# The second parameter is the driver id
# The third parameter is the filter to adapt
# Following parameters are optional and will be forwarded to the python script


if [ $# < 3 ]
  then
    echo "Error: The parameters are missing !"
    exit 1
fi


folder=$1
id=$2
filter_name=$3
shift
shift
shift
more_options=$@

./python/find_best_config.py --config-filters config/export_config.json \
                             --config-limits config/limits_config_debug.json \
                             --config-compare config/compare_config.json \
                             --section $filter_name \
                             --report $folder/$id.json \
                             --expected annotation/$id\_ref.json \
                             -o scores_$id.json \
                             $more_options

./python/graph_scores.py -i scores.json -c $filter_name

