set -e
set -x

# This script generate results and compare graphically the results with the expected annotations

# The first parameter is the path to the video processing results
# The second parameter is the driver id
# The third parameter is the client id

if ! [ $# -eq 3 ]
  then
    echo "Error: The parameters are missing !"
    exit 1
fi


./python/gen_results.py -i $1/$2.json --export-config config/export_config.json --mode window -o .

./python/show_results.py --up results_sdk.json --key-up confidence --down annotation/$3/$2_ref.json --key-down intensity --highlight speak

