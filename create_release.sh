#!/bin/bash

set -x
set -e

# The first parameter is the client id

if ! [ $# -eq 1 ]
then
    echo "Error: The parameters are missing !"
    exit 1
fi

commit_id=`git rev-parse HEAD | cut -c 1-8`

find -name __pycache__ | xargs rm -rf

rm -f report/*.json
rm -f report/*.js

RELEASE_DIR=scripts_$1_$commit_id

rm -rf $RELEASE_DIR
mkdir $RELEASE_DIR

sed 's/{ID}/'"$1"'/g' README.md > $RELEASE_DIR/README.md

cp -r --parents annotation/empty.json $RELEASE_DIR
cp -r --parents annotation/$1 $RELEASE_DIR
cp -r --parents config/ $RELEASE_DIR
cp -r --parents python/*.py $RELEASE_DIR
cp -r --parents python/$1 $RELEASE_DIR
cp -r --parents report/ $RELEASE_DIR
cp -r --parents script/*.sh $RELEASE_DIR
cp -r --parents script/$1 $RELEASE_DIR

zip -r $RELEASE_DIR.zip $RELEASE_DIR
