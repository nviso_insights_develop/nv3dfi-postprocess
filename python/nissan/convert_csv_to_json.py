#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.

"""
    Convert the PSA csv to a json with video analysis results
"""

import os
import sys
import copy
import argparse

from collections import OrderedDict

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))

from utils import utils


EMOTIONS = ['Joy', 'Suprise', 'Anger', 'Sorrow']


def convert_emotion(emotion_str):
    """ Convert emotion """

    emotion = 'none'

    if emotion_str == 'Anger':
        emotion = 'anger'
    elif emotion_str == 'Disgust':
        emotion = 'disgust'
    elif emotion_str == 'Fear':
        emotion = 'fear'
    elif emotion_str == 'Joy':
        emotion = 'happiness'
    elif emotion_str == 'Neutrality':
        emotion = 'neutrality'
    elif emotion_str == 'Sorrow':
        emotion = 'sadness'
    elif emotion_str == 'Suprise':
        emotion = 'surprise'

    return emotion


def read_nissan_csv(csv_filename):
    """ Return as a list the content of the CSV file """

    csv_content = utils.read_csv(csv_filename)

    csv_header = csv_content[:1]

    csv_results = csv_content[1:]

    return csv_header, csv_results


def extract_emotions(header, results):
    """ Extract emotion values """

    emotions = []

    start_time = float(results[0].split(';')[0])

    header_list = header[0].split(';')

    # Get emotion indexes
    start_index = 0
    count = 0
    for i, h in enumerate(header_list):
        print
        if h in EMOTIONS:
            if start_index == 0:
                start_index = i
            count += 1

    for i, line in enumerate(results):

        info = line.split(';')
        time = float(info[0])

        emotion_info = OrderedDict()
        emotion_info['time'] = round(time - start_time, 3)

        for j, emotion in enumerate(header_list[start_index:start_index+count]):

            emotion_info['emotion'] = convert_emotion(emotion)
            emotion_info['intensity'] = info[start_index+j]
            emotion_info['certainty'] = 0.5

            emotions.append(copy.deepcopy(emotion_info))

    return emotions


def convert_to_json(csv_filename, json_filename):
    """ Convert CSV to JSON """

    if json_filename == '':
        json_filename = csv_filename.replace('.csv', '.json')

    print("Ouptut: " + json_filename)

    header, results = read_nissan_csv(csv_filename)

    emotions = extract_emotions(header, results)

    dict_data = OrderedDict()
    dict_data['data'] = emotions

    utils.write_json(dict_data, json_filename, export_js=False)

    return


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", action="store", required=True, help="CSV input file")

    parser.add_argument("-o", "--output", action="store", default='', help="JSON output file")

    args = parser.parse_args()

    if not os.path.isfile(args.input):
        print("Error: The file ", args.input, " does not exist !!")
        return False

    convert_to_json(args.input, args.output)


if __name__ == '__main__':
    main()
