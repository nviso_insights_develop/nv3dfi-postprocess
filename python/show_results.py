#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" This is a result analysis """

# Usage ex:
#
# ./python/show_results.py --up results.json --key-up confidence \
#                          --down annotation/BRPR2012_ref.json --key-down intensity \
#                          --highlight speak


import argparse
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from utils import utils
from utils import graph

from collections import OrderedDict


def main():
    """ Entry point """

    parser = argparse.ArgumentParser()
    parser.add_argument("--up", action="store", required=True, help="Json results to display, up")
    parser.add_argument("--down", action="store", required=True,
                        help="Json results to display, down")
    parser.add_argument("--key-up", dest="key_up", action="store", required=True,
                        help="Key to use for top graph")
    parser.add_argument("--key-down", dest="key_down", action="store", required=True,
                        help="Key to use for down graph")
    parser.add_argument("--highlight", action="store", help="Key of area to highlight (ex: speak)")
    parser.add_argument("--output", action="store", help="Output image filename")

    args = parser.parse_args()

    results_up = utils.open_json(args.up)
    results_down = utils.open_json(args.down)

    timestamps = []
    emotions = OrderedDict()

    for emo_string in utils.EMOTIONS:
        emotions[emo_string] = []

    timestamps, emotions = graph.transform_to_bar_plot_data(results_up, timestamps, emotions,
                                                            args.key_up)

    timestamps, emotions = graph.transform_to_bar_plot_data(results_down, timestamps, emotions,
                                                            args.key_down, True)

    fig, ax = plt.subplots(nrows=1, ncols=1)

    # Normalize color to [0, 1]
    plot_colors = OrderedDict()
    for key in utils.EMOTION_COLORS:
        rgb = utils.EMOTION_COLORS[key]
        plot_colors[key] = (rgb[0]/255, rgb[1]/255, rgb[2]/255)

    for key, val in plot_colors.items():
        plt.fill(timestamps, emotions[key], color=val, label=key, linewidth=0.1)

    # Add highlight areas to graph
    if args.highlight:

        for result in results_down['data']:
            if args.highlight not in result:
                continue

            start_x = result['time']
            width = result['duration']

            ax.add_patch(patches.Rectangle((start_x, -1.01), width, 2.02,
                                           fill=True, color='red', alpha=0.1, edgecolor='red'))

    plt.title("Emotion Results (short sliding window) compared to annotations")
    plt.xlabel("Time (s)")
    plt.ylabel("Emotion (" + args.key_down + " / " + args.key_up + ")")
    plt.ylim(-1.1, 1.1)

    # Horizontal legend
    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
    #            fancybox=False, shadow=False, ncol=len(utils.EMOTIONS))

    # Vertical legend
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)

    if not args.output:
        plt.show()
    else:
        fig.set_dpi(100)
        fig.set_size_inches(20, 5)
        fig.savefig(args.output)


if __name__ == '__main__':
    main()
