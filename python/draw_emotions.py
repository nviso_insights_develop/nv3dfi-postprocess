#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.

"""
    Convert the PSA csv to a json with video analysis results
"""

import os
import argparse

from collections import OrderedDict
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.interpolate import interp1d
import numpy as np

from utils import utils


def process_emotions(emotions):

    timestamps = []
    emotions_f = []
    for i, em in enumerate(utils.EMOTIONS):
        emotions_f.append([])

    last_time = -1

    for i, info in enumerate(emotions['data']):

        t = info['time']

        if t != last_time:

            timestamps.append(t)
            last_time = t

            for j, em in enumerate(utils.EMOTIONS):
                emotions_f[j].append(0)

        index = utils.EMOTIONS.index(info['emotion'])

        if index < len(utils.EMOTIONS):
            intensity = float(info['intensity'])
            if intensity < 0:
                intensity = np.nan
            emotions_f[index][len(timestamps) - 1] = intensity

    return timestamps, emotions_f


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", action="store", required=True, help="Json input file")
    parser.add_argument("-ic", "--input-compare", dest="input_compare", action="store",
                        help="Json input file to compare to input")

    args = parser.parse_args()

    if not os.path.isfile(args.input):
        print("Error: The file ", args.input, " does not exist !!")
        return False

    if args.input_compare and (not os.path.isfile(args.input_compare)):
        print("Error: The file ", args.input_compare, " does not exist !!")
        return False

    emotions = utils.open_json(args.input)

    if args.input_compare:
        emotions_compare = utils.open_json(args.input_compare)

    timestamps, emotions_f = process_emotions(emotions)

    if args.input_compare:
        timestamps_compare, emotions_f_compare = process_emotions(emotions_compare)

    # Normalize color to [0, 1]
    plot_colors = {}
    for key in utils.EMOTION_COLORS:
        rgb = utils.EMOTION_COLORS[key]
        plot_colors[key] = (rgb[0]/255, rgb[1]/255, rgb[2]/255)

    max_y = 0
    for i, emotion in enumerate(utils.EMOTIONS):
        if np.nanmax(emotions_f[i]) > max_y:
            max_y = np.nanmax(emotions_f[i])

    multiplier = 1
    if args.input_compare:
        multiplier = 2

    # Results independent per emotion
    fig, axes = plt.subplots(len(utils.EMOTIONS) * multiplier, 1, sharex=True)

    for i, emotion in enumerate(utils.EMOTIONS):

        if args.input_compare:
            axe_y = axes[i*multiplier].twiny()

        axes[i*multiplier].plot(timestamps, emotions_f[i],
#                                color=plot_colors[emotion],
                                color=(1, 0, 0),
                                label=emotion, linewidth=2)

        if args.input_compare:

            axe_y.plot(timestamps_compare, [y * max_y for y in emotions_f_compare[i]],
                       color=plot_colors[emotion],
#                       color=(1, 0, 0),
                       label=emotion, linewidth=1)

            # Compare emotion on different y scale
            axes[(i*multiplier)+1].plot(timestamps_compare, emotions_f_compare[i],
                                        color=plot_colors[emotion],
                                        label=emotion, linewidth=2)

            max_y_2 = 1.1 * np.nanmax(emotions_f_compare[i])
            if max_y_2 < 0.1:
                max_y_2 = 0.1
            axes[(i*multiplier)+1].set_ylim(0, max_y_2)

        axes[i*multiplier].set_title("" + emotion, fontsize=10)
        axes[i*multiplier].set_ylim(0, max_y*1.1)
        if args.input_compare:
            axe_y.set_ylim(0, max_y*1.1)

    axes[int(len(utils.EMOTIONS)/2)*multiplier].set_ylabel("Confidence")
    axes[(len(utils.EMOTIONS)*multiplier)-1].set_xlabel("Time (s)")

    if True:

        fig.set_dpi(100)
        fig.set_size_inches(20, 15)
        fig.tight_layout()
        export_graph_path = "emotions.png"
        fig.savefig(export_graph_path)

    # One graph per emotion
    for i, emotion in enumerate(utils.EMOTIONS):

        fig, axes = plt.subplots(multiplier, 1, sharex=True)

        if args.input_compare:
            axe_y = axes[0].twiny()

        axes[0].plot(timestamps, emotions_f[i],
                     color=(1, 0, 0),
#                     color=plot_colors[emotion],
                     label=emotion, linewidth=2)

        if args.input_compare:

            axe_y.plot(timestamps_compare, [y * max_y for y in emotions_f_compare[i]],
                       color=plot_colors[emotion],
                       label=emotion, linewidth=2)

            # Compare emotion on different y scale
            axes[1].plot(timestamps_compare, [y * max_y for y in emotions_f_compare[i]],
                         color=plot_colors[emotion],
                         label=emotion, linewidth=2)

            max_y_2 = 1.1 * np.nanmax(emotions_f_compare[i]) * max_y
            if max_y_2 < 0.1:
                max_y_2 = 0.1
            axes[1].set_ylim(0, max_y_2)

        axes[0].set_title("" + emotion, fontsize=10)
        axes[0].set_ylim(0, max_y*1.1)
        if args.input_compare:
            axe_y.set_ylim(0, max_y*1.1)

        axes[0].set_ylabel("Confidence")
        axes[0].legend([emotion + ' provided'], loc=9)
        if args.input_compare:
            axe_y.legend([emotion + ' nViso'], loc=1)

        axes[1].set_ylabel("Confidence")
        axes[1].set_xlabel("Time (s)")
        axes[1].legend([emotion + ' nViso'], loc=1)

        if True:

            fig.set_dpi(100)
            fig.set_size_inches(20, 15)
            fig.tight_layout()
            export_graph_path = "emotions_" + emotion + ".png"
            fig.savefig(export_graph_path)


if __name__ == '__main__':
    main()
