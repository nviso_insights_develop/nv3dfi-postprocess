#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.

"""
    Convert the PSA csv to a json with video analysis results
"""

import os
import sys
import argparse

from collections import OrderedDict

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))

from utils import utils


CERTAINTY_KEY = 'Degré de certitude'
EMOTION_KEY = 'Emotion'
COMMENT_KEY = 'Commentaire'
INTENSITY_KEY = 'Intensité'
OCCLUSION_KEY = 'Occlusion'
VALENCE_KEY = 'Valence'
SPEAK_KEY = 'Parler'


def convert_range(value_str):
    """ Convert value from -2 to +2 in range [0:1] """

    value = 0

    value_str = value_str.lstrip(' ').replace('+ ', '+').replace('- ', '-').split(' ')[0]

    # Convert value
    if (value_str == '-2'):
        value = 0.2
    elif (value_str == '-1'):
        value = 0.4
    elif (value_str == '0'):
        value = 0.6
    elif (value_str == '+1'):
        value = 0.8
    elif (value_str == '+2'):
        value = 1

    return value


def convert_emotion(emotion_str):
    """ Convert emotion """

    emotion = 'none'

    if emotion_str == 'Colère':
        emotion = 'anger'
    elif emotion_str == 'Dégoût':
        emotion = 'disgust'
    elif emotion_str == 'Peur':
        emotion = 'fear'
    elif emotion_str == 'Joie':
        emotion = 'happiness'
    elif emotion_str == 'Neutre':
        emotion = 'neutrality'
    elif emotion_str == 'Tristesse':
        emotion = 'sadness'
    elif emotion_str == 'Surprise':
        emotion = 'surprise'

    return emotion


def read_psa_csv(csv_filename):
    """ Return as a list the content of the DOS CSV file """

    csv_content = utils.read_csv(csv_filename, 'utf-16', '\r\n')

    # Find end of header
    lastHeaderLineIndex = 0

    for i, line in enumerate(csv_content):

        if 'Start Time' in line:

            lastHeaderLineIndex = i

    csv_header = csv_content[:lastHeaderLineIndex+1]

    csv_results = csv_content[lastHeaderLineIndex+1:]

    return csv_header, csv_results


def extract_sections(results):
    """ Extract sections """

    sections = []

    start_time = results[0].split(';')[0]
    last_time = start_time
    section_info = OrderedDict()

    for i, line in enumerate(results):

        info = line.split(';')
        time = info[0]

        if float(time) != float(last_time):

            section_info['time'] = float(last_time)

            last_time = time
            sections.append(section_info)
            section_info = OrderedDict()

        key = info[1]
        data = info[2:]

        # Split to have speak areas independant of emotion and avoid to mess up duration
        if key == SPEAK_KEY:
            section_info_tmp = OrderedDict()
            section_info_tmp['time'] = float(time)
            section_info_tmp[key] = data
            sections.append(section_info_tmp)
        else:
            section_info[key] = data

    return sections


def sections_to_json(sections, json_filename):
    """ Convert the time sections to json """

    if len(sections) == 0:
        print("Error: no section to dump !!")
        return

    out_data = []

    for i, section in enumerate(sections):

        section_data = OrderedDict()

        section_data['time'] = section['time']

        for key in section:

            # Find comment
            if key == COMMENT_KEY:
                try:
                    comment = section[key][0]
                    section_data['comment'] = comment
                except IndexError:
                    pass  # Empty comment
                break  # No need to continue

            # Find duration
            if (key != 'time') and (len(section[key]) > 1):

                # Extract emotion duration if emotion is present otherwise no specific rule
                if ((EMOTION_KEY in section) and (key == EMOTION_KEY)) or \
                        (EMOTION_KEY not in section):

                    duration = section[key][-1]
                    to_sec = [3600, 60, 1]
                    duration = sum([a*b for a, b in zip(to_sec, map(float, duration.split(':')))])

                    section_data['duration'] = duration

            # Find speak
            if key == SPEAK_KEY:
                if section[key][0] == 'Oui':
                    section_data['speak'] = 'yes'
                break  # No need to continue

            # Find intensity
            if key.strip() == INTENSITY_KEY:
                section_data['intensity'] = convert_range(section[key][0])

            # Find certainty
            if key == CERTAINTY_KEY:
                section_data['certainty'] = convert_range(section[key][0])

            # Find emotion
            if key == EMOTION_KEY:
                emotion = convert_emotion(section[key][0])
                if emotion != 'none':
                    section_data['emotion'] = emotion

            # Find occlusion
            if OCCLUSION_KEY in key:
                occlusion = section[key][0]
                if occlusion == 'Oui':
                    occlusion = key.replace(OCCLUSION_KEY + ' ', '')
                    occlusion = occlusion.replace('frontale', 'frontal')
                    occlusion = occlusion.replace('droite', 'right')
                    occlusion = occlusion.replace('gauche', 'left')
                    section_data['occlusion'] = occlusion

        # Add only not empty sections
        keys_to_export = ['comment', 'speak', 'emotion', 'occlusion']
        for key in keys_to_export:
            if key in section_data:

                # Change order of keys
                ordered_data = OrderedDict()
                ordered_data['time'] = section_data['time']
                if 'duration' in section_data:
                    ordered_data['duration'] = section_data['duration']
                for k in section_data.keys():
                    if (k != 'time') and (k != 'duration'):
                        ordered_data[k] = section_data[k]

                out_data.append(ordered_data)
                break

    dict_data = {}
    dict_data['data'] = out_data

    utils.write_json(dict_data, json_filename, export_js=False)

    return


def convert_to_json(csv_filename, json_filename):
    """ Convert CSV to JSON """

    if json_filename == '':
        json_filename = csv_filename.replace('.csv', '.json')

    print("Ouptut: " + json_filename)

    header, results = read_psa_csv(csv_filename)

    sections = extract_sections(results)

    sections_to_json(sections, json_filename)

    return


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", action="store", required=True, help="CSV input file")

    parser.add_argument("-o", "--output", action="store", default='', help="JSON output file")

    args = parser.parse_args()

    if not os.path.isfile(args.input):
        print("Error: The file ", args.input, " does not exist !!")
        return False

    convert_to_json(args.input, args.output)


if __name__ == '__main__':
    main()
