#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Convert SM CSV with landmarks to JSON """


import os
import csv
import sys
import json
import copy
import argparse

from collections import OrderedDict

sys.path.append(os.path.join(os.path.join(os.path.abspath(os.path.dirname(__file__)), ".."), ".."))

from nv3dfi_scripts.bounding_box import point_3d, compute_bbox


def extract_landmarks(landmarks_points):

    left_eye_landmarks = [36, 37, 38, 39, 40, 41]
    right_eye_landmarks = [42, 43, 44, 45, 46, 47]
    nose_landmarks = [30]
    left_mouth_landmarks = [48]
    right_mouth_landmarks = [54]

    # make the result array
    result = []
    for i in range(5):
        p = point_3d()
        result.append(p)

    count = 0
    for point in landmarks_points:

        x = point[0]
        y = point[1]

        if (x != -1) and (y != -1):

            if(count in left_eye_landmarks):
                result[0].pt_x += float(x)
                result[0].pt_y += float(y)
                result[0].pt_z = 0

            if(count in right_eye_landmarks):
                result[1].pt_x += float(x)
                result[1].pt_y += float(y)
                result[1].pt_z = 0

            if(count in nose_landmarks):
                result[2].pt_x += float(x)
                result[2].pt_y += float(y)
                result[2].pt_z = 0

            if(count in left_mouth_landmarks):
                result[3].pt_x += float(x)
                result[3].pt_y += float(y)
                result[3].pt_z = 0

            if(count in right_mouth_landmarks):
                result[4].pt_x += float(x)
                result[4].pt_y += float(y)
                result[4].pt_z = 0

        count += 1

    result[0].pt_x = result[0].pt_x/len(left_eye_landmarks)
    result[0].pt_y = result[0].pt_y/len(left_eye_landmarks)
    result[1].pt_x = result[1].pt_x/len(left_eye_landmarks)
    result[1].pt_y = result[1].pt_y/len(left_eye_landmarks)

    return result


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", action="store", help="Input csv file", required=True)
    parser.add_argument("-o", action="store", help="Output json file", required=True)
    parser.add_argument("-s", type=float, help="Scale factor", default=1.0)
    parser.add_argument("--decimate", type=int, help="Decimate", default=sys.maxsize)

    args = parser.parse_args()

    csv_file = open(args.i, newline='\n')
    csv_content = csv.reader((line.replace('\0', '') for line in csv_file))
    csv_lines = []
    for row in csv_content:
        if row != []:
            csv_lines.append(row)
    csv_file.close()

    out_data = OrderedDict()
    out_data['frames'] = list()

    start_index = int(csv_lines[1][0])
    last_index = int(csv_lines[-1][0])
    start_time = float(csv_lines[1][1])
    dt = int(csv_lines[2][1]) - int(csv_lines[1][1])

    count = 0

    # Avoid gaps in landmark data, not necessary up to now
#    # Empty landmark
#    empty_lm = csv_lines[1]
#    for i, value in enumerate(empty_lm):
#        empty_lm[i] = 0

#    # Avoid gaps
#    for j, line2 in enumerate(csv_lines[1:]):

#        previous_index = start_index

#        for i, line in enumerate(csv_lines[2:]):

#            if int(line[0]) - 1 != previous_index:
#                empty_lm[0] = int(line[0]) - 1
#                empty_lm[1] = int(line[1]) - dt
#                csv_lines.insert(i+2, copy.deepcopy(empty_lm))
##                print(int(line[0]) - 1)
#                break

#            previous_index = int(line[0])

#        if previous_index == last_index:
#            print("DONE")
#            break

    decimate = args.decimate
    if decimate == 1:
        decimate = sys.maxsize

    for i, line in enumerate(csv_lines[1:]):

        if i % decimate == 0:
            continue

        frame = OrderedDict()

        face_data = list()

        face = OrderedDict()

        face['id'] = 0
        face['detection'] = OrderedDict()
        face['landmark'] = list()

        head_valid = int(line[3])

        landmarks = OrderedDict()
        landmarks['points'] = list()

        line_index = 4

        for i in range(0, 68):

            if (i == 60) or (i == 64):
                landmarks['points'].append([-1, -1])
                continue

            valid = int(line[line_index])
            lid = int(line[line_index + 1])
            x = float(line[line_index + 2])
            y = float(line[line_index + 3])

            if (valid != 0) and (head_valid == 1):
                # TODO: x offset
                landmarks['points'].append([(1280-x-(args.s*128*2))/args.s, y/args.s])
#                landmarks['points'].append([(1280-x)/args.s, y/args.s])
            else:
                landmarks['points'].append([-1, -1])

            line_index += 4

        landmarks_mirror = OrderedDict()
        landmarks_mirror['points'] = list()

        landmark_map = [[0,16],
                        [1,15],
                        [2,14],
                        [3,13],
                        [4,12],
                        [5,11],
                        [6,10],
                        [7,9],
                        [8,8],
                        [9,7],
                        [10,6],
                        [11,5],
                        [12,4],
                        [13,3],
                        [14,2],
                        [15,1],
                        [16,0],
                        [17,26],
                        [18,25],
                        [19,24],
                        [20,23],
                        [21,22],
                        [22,21],
                        [23,20],
                        [24,19],
                        [25,18],
                        [26,17],
                        [27,27],
                        [28,28],
                        [29,29],
                        [30,30],
                        [31,35],
                        [32,34],
                        [33,33],
                        [34,32],
                        [35,31],
                        [36,45],
                        [37,44],
                        [38,43],
                        [39,42],
                        [40,47],
                        [41,46],
                        [42,39],
                        [43,38],
                        [44,37],
                        [45,36],
                        [46,41],
                        [47,40],
                        [48,54],
                        [49,53],
                        [50,52],
                        [51,51],
                        [52,50],
                        [53,49],
                        [54,48],
                        [55,59],
                        [56,58],
                        [57,57],
                        [58,56],
                        [59,55],
                        [60,64],
                        [61,63],
                        [62,62],
                        [63,61],
                        [64,60],
                        [65,67],
                        [66,66],
                        [67,65]]

        for i in range(0, 68):
            landmarks_mirror['points'].append(landmarks['points'][landmark_map[i][1]])

        face['landmark'] = landmarks_mirror
        face['landmark']['type'] = 'lm68'

        bb = OrderedDict()
        bb.width = -1
        bb.height = -1
        bb.top_x = -1
        bb.top_y = -1

        if head_valid:
            sub_landmarks = extract_landmarks(landmarks_mirror['points'])
            bb = compute_bbox(sub_landmarks, 1280, 1024, nose_dist_fraction=0.75, multiplier=2.2)

        face['detection']['location'] = OrderedDict()
        face['detection']['location']['h'] = bb.width if bb.width < 1280 else -1
        face['detection']['location']['w'] = bb.height if bb.height < 1024 else -1
        face['detection']['location']['x'] = bb.top_x if bb.width < 1280 else -1
        face['detection']['location']['y'] = bb.top_y if bb.height < 1024 else -1

        face_data.append(face)

        frame['face_data'] = face_data

        frame['video_pos_ms'] = (float(line[1]) - start_time) / 1000
        frame['video_pos_frame'] = (int(line[0]) - start_index)
        frame['video_count_frame'] = count

        previous_index = frame['video_pos_frame']

        out_data['frames'].append(frame)
        count += 1

    with open(os.path.join(args.o), "w") as out_file:
        json.dump(out_data, out_file, indent=4)
