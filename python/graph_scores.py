#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Show the content of a score file (output of find_best_config.py) graphicaly

    One graph for score and another graph for configurable values

"""

# Usage ex:
#
# ./python/gen_results.py -i scores.json \
#                         -c bias_removal,time_constant,happiness


import argparse
import matplotlib.pyplot as plt
import numpy as np

from utils import utils


class Cursor(object):
    """ Show a cursor on x and y

        https://matplotlib.org/gallery/misc/cursor_demo_sgskip.html?highlight=cursor
    """

    def __init__(self, ax, x, y):

        self.ax = ax
        self.lx = ax.axhline(color='k', lw=0.5)
        self.ly = ax.axvline(color='k', lw=0.5)

        self.x = x
        self.y = y

        # Text location
        self.txt = ax.text(0.7, 0.9, '', transform=ax.transAxes)

    def mouse_move(self, event):

        if not event.inaxes:
            return

        x, y = event.xdata, event.ydata

        indx = min(np.searchsorted(self.x, [x])[0], len(self.x) - 1)
        x = self.x[indx]
        y = self.y[indx]

        # Update the lines position
        self.lx.set_ydata(y)
        self.ly.set_xdata(x)

        self.txt.set_text('x=%1.2f, y=%1.2f' % (x, y))
        plt.draw()


def main():
    """ Entry point """

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", action="store", required=True,
                        help="Scores json file")
    parser.add_argument("-c", "--compare", action="store", required=False,
                        help="Compare score to value increase, comma separated list")

    args = parser.parse_args()

    results = utils.open_json(args.input)
    keys = []
    if args.compare:
        keys = args.compare.split(',')

    values_x = []
    values_y_list = []

    values_y_list.append([])
    values_y_list.append([])

    value = results['data'][0]['config']
    for key in keys:
        value = value[key]

    if args.compare:
        for k in value:
            values_y_list.append([])

    for i, result in enumerate(results['data']):

        values_x.append(i)
        values_y_list[0].append(result['match_rate'])
        values_y_list[1].append(result['score'])

        if args.compare:

            value = result['config']
            for key in keys:
                value = value[key]

            for j, k in enumerate(value):
                values_y_list[j+2].append(value[k])

    colors = ['orange', 'r']
    colors += ['magenta', 'gold', 'deepskyblue', 'blueviolet', 'limegreen', 'dimgrey', 'silver']

    labels = ['score', 'match_rate']
    for emotion in utils.EMOTIONS:
        labels.append(emotion)

    fig, axes = plt.subplots(nrows=3, ncols=1, sharex=True)

    axes[0].plot(values_x, values_y_list[0], color=colors[0], label=labels[0])
    axes[1].plot(values_x, values_y_list[1], color=colors[1], label=labels[1])

    for i, values_y in enumerate(values_y_list):
        if i > 1:
            axes[2].plot(values_x, values_y, color=colors[i], label=labels[i])

    max_value = 0
    for values in values_y_list[2:]:
        value = max(values)
        if value > max_value:
            max_value = value

    plt.xlabel("index")
    axes[0].set_ylabel("match_rate")
    axes[1].set_ylabel("score")
    axes[2].set_ylabel(args.compare)
    plt.xlim(0, values_x[-1])
    axes[0].set_ylim(min(values_y_list[0])-min(values_y_list[0])*0.1,
                     max(values_y_list[0])+max(values_y_list[0])*0.1)
    axes[1].set_ylim(0, max(values_y_list[1])+max(values_y_list[1])*0.1)
    axes[2].set_ylim(0, max_value+max_value*0.1)

    cursor = Cursor(axes[1], values_x, values_y_list[1])
    plt.connect('motion_notify_event', cursor.mouse_move)

    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=len(values_y_list), mode="expand", borderaxespad=0.)

    axes[2].legend(bbox_to_anchor=(1.01, 1.3), loc=2, borderaxespad=0.)

    plt.show()


if __name__ == '__main__':
    main()
