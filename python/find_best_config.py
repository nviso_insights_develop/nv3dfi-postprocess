#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Find the best configuration, for filters

    Try all the possible configuration in the given range and find the configuration with
    the smaller score.

    A small score indicates that most of the errors are done on value with low
    intensity and certainty.

    Multiple parameters are available to reduce the computation time:
        - Start and end time
        - Section filter

    The output of this script is a json file, this json file can be analyzed using graph_scores.py

"""

# Usage ex:
#
# ./python/find_best_config.py --config-filters config/export_config.json \
#                              --config-limits config/limits_config.json \
#                              --config-compare config/compare_config.json \
#                              --report BRPR2012_sdk_2.json \
#                              --expected annotation/BRPR2012_ref.json \
#                              --section bias_removal,time_constant


import sys
import copy
import math
import time
import signal
import argparse
import numpy as np

from utils import utils
from utils import filters
from utils import export_results
from utils import compare

from collections import OrderedDict


def find_all_values(config, reverse):
    """ Find all the possible values for a given config """

    minimum = float(config['minimum'])
    maximum = float(config['maximum']) + 0.001
    step = float(config['step'])

    values = np.arange(minimum, maximum, step)

    # Reverse order
    if reverse:
        values = values[::-1]

    return values


def init_combinations(list1, list2):
    """ Find all combinations between two lists containing values """

    out = []

    for i in list1:
        for j in list2:
            out.append([i, j])

    return out


def combinations(list1, list2):
    """ Find all combinations between two lists. The first list is a list of list """

    out = []

    for i in list1:
        for j in list2:
            out.append(i + [j])

    return out


def section_combinations(values, count):
    """ Return all the combination for a section """

    comb = init_combinations(values, values)

    for i in range(1, count-1):
        comb = combinations(comb, values)

    return comb


def compute_score(results):
    """ Compute the score of the given results """

    limits = [0.2, 0.4, 0.6, 0.8, 1]

    def count_errors(key, list_keys):

        total = 0

        count_key = OrderedDict()
        count_key_errors = OrderedDict()

        for i in range(0, len(list_keys)):

            count_key[list_keys[i]] = 0
            count_key_errors[list_keys[i]] = 0

        for value in results['data']:

            if (key in value) and (value[key] != ''):

                total += 1

                count_key[value[key]] += 1

                if ('match' in value) and (value['match'] != ''):

                    if value['match'] == 'False':
                        count_key_errors[value[key]] += 1

        return count_key, count_key_errors, total

    count_emotion, count_emotion_errors, total = count_errors('emotion', utils.EMOTIONS)
    count_intensity, count_intensity_errors, total = count_errors('intensity', limits)
    count_certainty, count_certainty_errors, total = count_errors('certainty', limits)

    score = 0

    count_error = 0
    for emotion in utils.EMOTIONS:
        count_error += count_emotion_errors[emotion]
    # print(count_error, "/", total)

    for limit in limits:
        score += (count_intensity_errors[limit] * limit) + (count_certainty_errors[limit] * limit)

    match_rate = 0
    if total > 0:
        match_rate = ((total - count_error) / total) * 100

    if count_error == total:
        print("All wrong", score, match_rate)

    return score, match_rate


def config_diff(config, previous_config):
    """ Compare two end dict and return the list of keys where values are different """

    diff_indexes = []

    if not isinstance(config, dict):

        if config != previous_config:

            for i, e in enumerate(utils.EMOTIONS):
                diff_indexes.append(i)
    else:

        for i, key in enumerate(config):

            # print(config[key], "!=", previous_config[key])
            if config[key] != previous_config[key]:
                diff_indexes.append(i)

    return diff_indexes


def run_test(emotions, timestamps, fps, export_config, expected, compare_config,
             section_keys, start_time, end_time, test_index,
             previous_config, previous_emotions_b, previous_results):
    """ Return the score using the given config """

    verbose = False

    if verbose:
        start = time.time()

    diff_indexes = []
    if test_index == 0:
        # Force to compute all the first time
        for i, e in enumerate(utils.EMOTIONS):
            diff_indexes.append(i)
    else:

        export_config_section = export_config
        previous_config_section = previous_config
        for key in section_keys:
            export_config_section = export_config_section[key]
            previous_config_section = previous_config_section[key]

        diff_indexes = config_diff(export_config_section, previous_config_section)

    emotions_b = copy.deepcopy(previous_emotions_b)

    # print(test_index)

    for i, index in enumerate(diff_indexes):

        previous_neutrality = None
        if (test_index > 0) and (i > 0):
            previous_neutrality = emotions_b[utils.NEUTRALITY_INDEX]

        emotions_b_tmp = filters.remove_bias(emotions, export_config, fps, utils.EMOTIONS[index],
                                             previous_neutrality)

        # Copy results one by one
        for j, data in enumerate(emotions_b_tmp):
            if data is not None:
                emotions_b[j] = data

    if verbose:
        end = time.time()
        print("Time bias:", int((end - start) * 1000), "ms")
        start = time.time()

    results = export_results.sliding_window(timestamps, emotions_b, export_config)

    if verbose:
        end = time.time()
        print("Time window:", int((end - start) * 1000), "ms")
        start = time.time()

    results_matches = compare.get_matches_results(results, expected, compare_config,
                                                  start_time, end_time)

    score, match_rate = compute_score(results_matches)

    if verbose:
        end = time.time()
        print(">> Time:", int((end - start) * 1000), "ms")

    return score, match_rate, emotions_b, results


def main():
    """ Entry point """

    parser = argparse.ArgumentParser()
    parser.add_argument("--config-filters", dest="export_config", action="store", required=True,
                        help="Json config file")
    parser.add_argument("--config-limits", dest="limits_config", action="store", required=True,
                        help="Json limits config file")
    parser.add_argument("--config-compare", dest="compare_config", action="store", required=True,
                        help="Json limits config file")
    parser.add_argument("--report", action="store", required=True,
                        help="Json video processing report file")
    parser.add_argument("--expected", action="store", required=True,
                        help="Expected json results")
    parser.add_argument("--section", action="store", required=True,
                        help="Section to change, comma separated list")
    parser.add_argument("--start-time", dest="start_time", type=float, default=0,
                        help="Video start time in seconds")
    parser.add_argument("--end-time", dest="end_time", type=float, default=math.inf,
                        help="Video end time in seconds")
    parser.add_argument("--filter", action="store",
                        help="Filter values to adapt in given section, comma separated list")
    parser.add_argument("--reverse", action="store_true", default=False,
                        help="Reverse order of combination values")
    parser.add_argument("-o", "--output", action="store", required=True, help="Output json results")

    args = parser.parse_args()

    export_config = utils.open_json(args.export_config)
    limits_config = utils.open_json(args.limits_config)
    compare_config = utils.open_json(args.compare_config)
    report = utils.open_json(args.report)
    expected = utils.open_json(args.expected)

    config = copy.deepcopy(export_config)

    values = []
    it_count = 1
    config_section = None

    section_keys = []
    if args.section:
        section_keys = args.section.split(',')

    config_section = config
    limits_config_section = limits_config
    for key in section_keys:
        config_section = config_section[key]
        limits_config_section = limits_config_section[key]

    # Get a pointer to the config to test
    if (section_keys[-1] == 'time_constant'):
        it_count = len(utils.EMOTIONS)
    elif (section_keys[-1] == 'percentage_to_keep'):
        it_count = len(utils.EMOTIONS)
    elif (section_keys[-1] == 'limit'):
        it_count = len(utils.EMOTIONS)
    elif (section_keys[-1] == 'step'):
        it_count = 1
    elif (section_keys[-1] == 'window_size'):
        it_count = len(utils.EMOTIONS)
    else:
        print("Error: invalid section [" + args.section + "]")
        sys.exit(1)

    values = find_all_values(limits_config_section, args.reverse)

    filters = None

    # Filter to reduce the number of possibilities to test
    if args.filter and (it_count > 1):
        filters = args.filter.split(',')
        it_count = len(filters)

    # TODO: for some bias_removal options it's not necessary to change the neutrality filters
    #       has it's not executed on this emotion

    comb = values
    if it_count > 1:
        comb = section_combinations(values, it_count)

    output_data = OrderedDict()
    output_data['data'] = []

    print("LEN", len(comb))

    timestamps, emotions, missing = utils.parse_video_report(report)

    dt = timestamps[1] - timestamps[0]
    fps = np.around(1 / dt)
    print("FPS:", fps)

    # Analyze only a part of the video
    start_index = 0
    if (args.start_time > 0):
        start_index = utils.convert_time_to_index(args.start_time, timestamps)
    end_index = len(timestamps)
    if (args.end_time > 0):
        end_index = utils.convert_time_to_index(args.end_time, timestamps)
    timestamps = timestamps[start_index:end_index]
    emotions = emotions[:, start_index:end_index]
    print("Start time", args.start_time, "[", start_index, "]")
    print("End time", args.end_time, "[", end_index, "]")

    best_config_index = 0
    best_score = math.inf

    previous_config = copy.deepcopy(config)
    previous_emotions_b = {}
    previous_results = {}

    # Export the computed results to output file
    def signal_handler(signal, frame):

        print('    Abort CTRL+C')

        if args.output:
            utils.write_json(output_data, args.output)
            utils.write_json(output_data['data'][best_config_index], "best_config.json")

        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)

    for i, comb_values in enumerate(comb):

        # Patch the config with new values
        if isinstance(config_section, dict):

            for j, key in enumerate(config_section.keys()):

                if filters and (key in filters):

                    for k, f in enumerate(filters):
                        if key == f:
                            if len(filters) == 1:
                                config_section[key] = comb_values
                            else:
                                config_section[key] = comb_values[k]

                if not filters:
                    config_section[key] = comb_values[j]
        else:
            config[args.section] = comb_values

        # print(">>", config_section)

        # Run the test
        ret = run_test(emotions, timestamps, fps, config, expected, compare_config,
                       section_keys, args.start_time, args.end_time, i,
                       previous_config, previous_emotions_b, previous_results)

        score = ret[0]
        match_rate = ret[1]
        previous_emotions_b = ret[2]
        previous_results = ret[3]

        previous_config = copy.deepcopy(config)

        if score < best_score:
            print("Score:", score, "i:", i)
            best_config_index = i
            best_score = score
            output_data['best_score'] = best_score

        result = OrderedDict()
        result['score'] = score
        result['match_rate'] = match_rate
        result['config'] = copy.deepcopy(config)

        output_data['data'].append(result)

        if i % 100 == 0:
            if args.output:
                utils.write_json(output_data['data'][best_config_index], "best_config.json")
                utils.write_json(output_data, args.output)

        if i % 1000 == 0:
            print(">>", i)

    # print("Best config:")
    # print(json.dumps(output_data['data'][best_config_index], indent=4))

    if args.output:

        utils.write_json(output_data, args.output)
        utils.write_json(output_data['data'][best_config_index], "best_config.json")


if __name__ == '__main__':
    main()
