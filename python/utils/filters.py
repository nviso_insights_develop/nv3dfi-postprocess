#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Filter utilities """

import copy
import numpy as np

from utils import utils


def lowpass_filter(data, fps, tc):
    """ Low pass filter """

    if tc == 0:
        print("Low pass filter is disabled for this data")
        return copy.deepcopy(data)

    # print(">> tc: " + str(tc) + " fps: " + str(fps))

    length = len(data)

    lowpass2 = lowpass = 0
    out = np.zeros(length)

    for i in range(0, length):

        lowpass = lowpass + (data[i] - lowpass) * ((1/tc) / fps)
        lowpass2 = lowpass2 + (lowpass - lowpass2) * ((1/tc) / fps)

        out[i] = lowpass2

    return out


def highpass_filter(data, fps, tc, percentage_to_keep, limit):
    """ High pass filter """

    if (tc == 0) or (percentage_to_keep == 1):
        print("High pass filter is disabled for this data")
        return copy.deepcopy(data)

    # print(">> tc: " + str(tc) + " percent: " + str(percentage_to_keep) + " fps: " + str(fps))

    length = len(data)

    state = 0

    lowpass = lowpass_filter(data, fps, tc)

    out = np.zeros(length)

    for i in range(0, length):

        # Apply the filter only on small values
        if data[i] >= 0.5:
            out[i] = data[i]
            continue

        if lowpass[i] > limit:
            lowpass[i] = limit

        state = data[i] - lowpass[i]
        if state < 0:
            state = 0

        out[i] = (data[i] * percentage_to_keep) + (state * (1 - percentage_to_keep))

    return out


def remove_bias(emotions, export_config, fps, emotion_filter=None, previous_neutrality=None):
    """ Remove bias using an high pass filter and add to neutral

        The emotion_filter parameter can be used with previous_neutrality parameter to apply bias
        filter only on a given emotion.
    """

    out = [None] * len(emotions)

    neutrality = 'neutrality'
    neutrality_index = utils.NEUTRALITY_INDEX

    if emotion_filter and (previous_neutrality is not None):
        out[neutrality_index] = copy.deepcopy(previous_neutrality)
    else:
        out[neutrality_index] = copy.deepcopy(emotions[neutrality_index])

    if emotion_filter == neutrality:
        return out

    for i, values in enumerate(emotions):

        emotion = utils.EMOTIONS[i]

        if emotion == neutrality:
            continue

        if emotion_filter and (emotion != emotion_filter):
            continue

        tc = export_config['bias_removal']['time_constant'][emotion]
        percent = export_config['bias_removal']['percentage_to_keep'][emotion]

        limit = export_config['bias_removal']['limit'][emotion]

        # Remove bias
        out[i] = highpass_filter(emotions[i], fps, tc, percent, limit)

        # Add everything removed to neutrality
        length = len(emotions[i])
        for j in range(0, length):
            out[neutrality_index][j] += emotions[i][j] - out[i][j]

    # Return filtered results for all emotions
    return out
