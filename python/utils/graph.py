#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Graph utilities """


import copy


def transform_to_bar_plot_data(results, timestamps_in, emotions_in, export_key, down_plot=False):
    """ Extract the data from the json and format to bar plot """

    def add_empty_point_to_other_emotions(emotions, emotion):
        """ Set line to zero for all emotions except the primary emotion """

        for key in emotions:
            if key != emotion:
                emotions[key].append(0)

    timestamps = copy.deepcopy(timestamps_in)
    emotions = copy.deepcopy(emotions_in)

    for emo_string in emotions.keys():
        emotions[emo_string].append(0)

    timestamps.append(0)

    coef = 1
    if down_plot:
        coef = -1

    for result in results['data']:

        if 'emotion' in result:

            emotion = result['emotion']

            if emotion == '':
                print("Error: Invalid emotion !")
                print(result)
                continue

            timestamps.append(result['time'])
            emotions[emotion].append(0)
            add_empty_point_to_other_emotions(emotions, emotion)

            timestamps.append(result['time'] + 0.001)
            emotions[emotion].append(coef * result[export_key])
            add_empty_point_to_other_emotions(emotions, emotion)

            timestamps.append(result['time'] + result['duration'] - 0.001)
            emotions[emotion].append(coef * result[export_key])
            add_empty_point_to_other_emotions(emotions, emotion)

            timestamps.append(result['time'] + result['duration'])
            emotions[emotion].append(0)
            add_empty_point_to_other_emotions(emotions, emotion)

    return timestamps, emotions

