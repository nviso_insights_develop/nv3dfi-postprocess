#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Compare utilities """


import copy
import math

from collections import OrderedDict


def get_matches_results(results, expected_results, config, start_time=0, end_time=math.inf):
    """ Return results json with matches """

    dict_data = {'data': []}

    if not isinstance(results, list):
        results = [results]

    for i in range(0, len(results)):

        if len(results[i]['data']) == 0:
            print("Error: the results are empty !")

    for expected in expected_results['data']:

        if 'emotion' not in expected:
            continue

        if expected['time'] < start_time:
            continue
        if expected['time'] > end_time:
            break

        time_diff = 0
        duration_diff = 0

        margin = config['margin'][expected['emotion']]
        exp_end_time = expected['time'] + expected['duration']
        min_time = expected['time'] - margin
        max_time = exp_end_time + margin

        result = copy.deepcopy(expected)

        # print(">>", min_time, " ", max_time)

        # Find match in results
        for i in range(0, len(results)):

            match = False

            for res in results[i]['data']:

                # Stop condition
                if res['time'] > max_time:
                    break

                res_end_time = res['time'] + res['duration']

                # If the emotion is (partially) present in the exected result
                if (res['time'] >= min_time) or \
                   ((res_end_time >= min_time) and (res_end_time <= max_time)) or \
                   ((expected['time'] >= res['time']) and (exp_end_time <= res_end_time)):

                    # print(res['time'], " ", res['duration'])
                    # print(res['emotion'], '==', expected['emotion'])

                    if res['emotion'] == expected['emotion']:

                        time_diff = res['time'] - expected['time']
                        duration_diff = res['duration'] - expected['duration']

                        match = True
                        break

            # TODO: extract from the configuration file
            types = ["short", "middle", "long"]

            match_key = 'match'
            if len(results) > 1:
                match_key = 'match_' + types[i]
            result[match_key] = str(match)

        if match and (len(results) == 1):
            result['diff'] = OrderedDict()
            result['diff']['time'] = time_diff
            result['diff']['duration'] = duration_diff

        dict_data['data'].append(result)

    return dict_data


def get_section_comments(expected_results):
    """ Get the list of comments for each section """

    comments = []

    for result in expected_results['data']:

        if 'comment' in result:

            comment = result['comment']

            if comment.startswith('Debut'):

                comment = comment.replace('Debut ', '')

                if comment == 'recording':
                    continue

                comments.append(comment)

    return comments

