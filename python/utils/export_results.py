#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Export results utilities """

import math
import copy
import numpy as np

from utils import utils

from collections import OrderedDict


def sliding_window(timestamps, emotions, config, config_type="short"):
    """ Export emotions using a sliding window """

    if (config is None) or (config == {}):
        print("\nError: config file parameter is missing !!\n")
        return None

    dt = timestamps[1] - timestamps[0]
    # print("dt: " + str(dt))

    step = int(np.around((config['step']) / dt))
    # print("Step: " + str(step))

    tot_emotions = []

    # Find the primary first emotion
    best_emotion = ''
    best_value = 0

    for i, values in enumerate(emotions):

        emotion = utils.EMOTIONS[i]

        if emotions[i][0] > best_value:
            best_value = emotions[i][0]
            best_emotion = emotion

    previous_data = OrderedDict()
    previous_data['time'] = timestamps[0]
    previous_data['duration'] = 0
    previous_data['confidence'] = best_value
    previous_data['emotion'] = best_emotion
    previous_count = 1

    if config_type not in config['window_size']:
        return None

    for i, t in enumerate(timestamps[::step]):

        best_emotion = ''
        best_value = 0

        for j, values in enumerate(emotions):

            emotion = utils.EMOTIONS[j]

            window_size = config['window_size'][config_type][emotion]
            nb_frames = int(np.around(window_size / dt))

            # Reduce window size when close to beginning
            if nb_frames > (i*step)+1:
                nb_frames = (i*step)+1

            if nb_frames == 0:
                continue

            # Get emotion window values, from current time, back to window size
            last_index = ((i+1)*step)
            emotion_area = emotions[j][last_index-nb_frames:last_index]

            tot_value = 0
            for value in emotion_area:
                tot_value += value

            tot_value /= nb_frames

            if tot_value > best_value:
                best_value = tot_value
                best_emotion = emotion

        # Restart the previously started section, if empty
        if (previous_data['emotion'] == '') and (best_value > 0):

            # Start new section
            previous_data['emotion'] = best_emotion
            previous_data['confidence'] = best_value
            previous_data['time'] = t
            previous_count = 1

        # Continue with the same emotion
        if (best_emotion == previous_data['emotion']) and (best_value > 0):

            previous_data['confidence'] += best_value
            previous_count += 1

        else:

            # Nothing to close
            if previous_data['emotion'] == '':
                continue

            # Close previous section
            previous_data['duration'] = t - previous_data['time']
            previous_data['confidence'] /= previous_count
            if previous_data['confidence'] < 0.2:
                previous_data['confidence'] = 0
            tot_emotions.append(copy.deepcopy(previous_data))

            # Start new section
            previous_data['emotion'] = best_emotion
            previous_data['confidence'] = best_value
            previous_data['time'] = t
            previous_count = 1

    # Close the last section
    if previous_data['emotion'] != '':

        previous_data['duration'] = t - previous_data['time']
        previous_data['confidence'] /= previous_count
        tot_emotions.append(copy.deepcopy(previous_data))

    dict_data = {}
    dict_data['data'] = tot_emotions

    return dict_data


def get_emotion_areas_using_primary(timestamps, primary_emotions, emotion_strings):
    """ Export primary emotions """

    previous_emotion = ''
    start_time = 0
    emotion_confidence = 0

    out_data_tmp = []

    for i, emotion in enumerate(emotion_strings):

        for j, t in enumerate(timestamps):

            confidence = primary_emotions[i][j]

            if (((confidence == 0) or (math.isnan(confidence))) and emotion == previous_emotion):

                # Close section
                data = OrderedDict()
                data['time'] = start_time
                data['duration'] = t - start_time
                data['confidence'] = emotion_confidence
                data['emotion'] = emotion

                out_data_tmp.append(data)

                previous_emotion = ''
                emotion_confidence = 0

            if confidence > 0:

                if emotion != previous_emotion:
                    # Start section
                    start_time = t
                    previous_emotion = emotion

                emotion_confidence = (0.5 * emotion_confidence) + (0.5 * confidence)

        if previous_emotion != '':
            # Close section
            data = OrderedDict()
            data['time'] = start_time
            data['duration'] = t - start_time
            data['confidence'] = emotion_confidence
            data['emotion'] = emotion

            out_data_tmp.append(data)

    # Sort data in the list per time
    out_data = []
    while len(out_data_tmp) != 0:

        smaller_time = out_data_tmp[0]['time']
        next_index = 0

        for i, data in enumerate(out_data_tmp):

            if data['time'] < smaller_time:
                smaller_time = data['time']
                next_index = i

        out_data.append(out_data_tmp[next_index])
        out_data_tmp.pop(next_index)

    dict_data = {}
    dict_data['data'] = out_data

    return dict_data


def get_average_emotion(timestamps, emotions, period=math.inf, ignore_neutrality=True):
    """ Get emotion average for a given period of time

        period parameter is a time in minutes, default is to process the whole video
    """

    seconds = timestamps[-1]
    if period != math.inf:
        seconds = period * 60

    average_values = {}
    for emotion in utils.EMOTIONS:
        average_values[emotion] = 0

    output_data = OrderedDict()
    output_data['max'] = []
    output_data['percent'] = []

    count = 0

    for j, t in enumerate(timestamps):

        empty = True
        for i, emotion in enumerate(utils.EMOTIONS):
            average_values[emotion] += emotions[i][j]
            if emotions[i][j] != 0:
                empty = False

        if not empty:
            count += 1

        if t >= seconds:
            seconds += period * 60

            percent_values = OrderedDict()

            max_average = 0
            max_emotion = None
            for i, emotion in enumerate(utils.EMOTIONS):

                percent_values[emotion] = average_values[emotion] / count

                if ignore_neutrality and (i == utils.NEUTRALITY_INDEX):
                    continue

                if average_values[emotion] > max_average:
                    max_average = average_values[emotion]
                    max_emotion = emotion

            for i, emotion in enumerate(utils.EMOTIONS):
                average_values[emotion] = 0

            count = 0

            tot = 0
            for i, emotion in enumerate(utils.EMOTIONS):
                tot += percent_values[emotion]

            output_data['max'].append(max_emotion)
            output_data['percent'].append(percent_values)

    return output_data


def get_non_match_averages(results_all, match_key):

    output_data = OrderedDict()

    emotion_non_match = OrderedDict()
    emotion_count = OrderedDict()
    for emotion in utils.EMOTIONS:
        emotion_non_match[emotion] = 0
        emotion_count[emotion] = 0

    for data in results_all['data']:

        if match_key in data:

            emotion = data['emotion']

            if emotion == "":
                continue

            match = data[match_key]

            emotion_count[emotion] += 1

            if match == 'False':
                emotion_non_match[emotion] += 1

    for emotion in utils.EMOTIONS:
        if emotion_count[emotion] > 0:
            emotion_non_match[emotion] /= emotion_count[emotion]

    output_data['percent'] = emotion_non_match

    return output_data
