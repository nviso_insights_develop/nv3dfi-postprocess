#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Utilities """

import os
import re
import json
import numpy as np

from collections import OrderedDict


EMOTIONS = ["happiness", "surprise", "sadness", "disgust", "fear", "anger", "neutrality"]
NEUTRALITY_INDEX = 6

EMOTION_COLORS = OrderedDict()
EMOTION_COLORS["happiness"] = (217, 12, 62)
EMOTION_COLORS["surprise"] = (253, 196, 29)
EMOTION_COLORS["sadness"] = (77, 189, 200)
EMOTION_COLORS["disgust"] = (130, 76, 152)
EMOTION_COLORS["fear"] = (134, 188, 41)
EMOTION_COLORS["anger"] = (38, 38, 38)
EMOTION_COLORS["neutrality"] = (178, 176, 161)


def open_json(filename):
    """ Return a dictionary from a json file """

    if filename is None:
        return None

    with open(filename) as json_data:
        return json.load(json_data, object_pairs_hook=OrderedDict)


def write_json(dict_data, filename, verbose=True, export_js=True):
    """ Write a dictionary to a json file """

    if verbose:
        print("Info: write json file: " + filename)

    with open(filename, 'w', encoding='utf-8') as json_file:
        json.dump(dict_data, json_file, indent=4)

    if export_js:

        lines = []

        with open(filename, 'r', encoding='utf-8') as json_file:
            lines = json_file.readlines()

        with open(filename.replace('.json', '.js'), 'w', encoding='utf-8') as js_file:
            # Transform the json file to a javascript file
            lines[0] = "var json_" + filename.replace('.json', '').split(os.path.sep)[-1] + " = " + lines[0]
            lines[-1] += ";"
            js_file.writelines(lines)


def parse_video_report(report):
    """ Exract emotions and timestamps from video processing results """

    frame_count = len(report["frames"])
    print("Frame count:", frame_count)

    nb_emotions = len(EMOTIONS)

    timestamps = np.zeros(frame_count)
    emotions = {}

    for emo_string in EMOTIONS:
        emotions[emo_string] = np.zeros(frame_count)

#    for emo_string in EMOTIONS:
#        emotions[emo_string] = np.full(frame_count, np.nan)

    # Extract results from the json
    missing = []
    for i, frame in enumerate(report["frames"]):
        timestamps[i] = frame["video_pos_ms"]/1000
        lastval = [0] * nb_emotions
        for k, emo_string in enumerate(EMOTIONS):
            if ("face_data" in frame) and (frame["face_data"] is not None) and (len(frame["face_data"]) > 0) and ("emotion" in frame["face_data"][0]):
                lastval[k] = frame["face_data"][0]["emotion"]['confidence'][emo_string]
            else:
                missing.append(i)
            emotions[emo_string][i] = lastval[k]

    # Convert from dict to array
    emo = np.zeros([nb_emotions, frame_count])
    for i, estr in enumerate(EMOTIONS):
        emo[i] = emotions[estr]

    return timestamps, emo, missing


def convert_time_to_index(time, timestamps):
    """ Find the index of the given time in timestamps """

    for i, t in enumerate(timestamps):

        if t > time:
            return i

    return len(timestamps)


def read_csv(csv_filename, encoding='utf-8', line_end='\n'):
    """ Return as a list the content of the DOS CSV file """

    csv_file = open(csv_filename, 'rb')
    list_csv = csv_file.read().decode(encoding).split(line_end)
    csv_content = []
    for line in list_csv:

        line = re.sub(r'\t+', ';', line)

        if (line != ';') and (line != ''):

            if line[-1] == ';':
                line = line[:-1]
            csv_content.append(line)

    csv_file.close()

    return csv_content

