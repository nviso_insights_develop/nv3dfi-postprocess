#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Genrerate results for comparison

    This script support two output formats:

        1. Results used for HTML report, the results are compared with the annotations
        2. Results for graphical analysis, the results are not compared with annotations

    This script is divided as follow:

        - Remove bias using an high pass filter
        - Extract time section using a sliding window
        - Compare to expected annotations
        - Patch results with more information (time diff, speak)
        - Show results graphically (optional)

    The filter operations are configurable using a json configuration file

"""

# Usage ex:
#
# ./python/gen_results.py -i BRPR2012_sdk_2.json \
#                         --export-config config/export_config.json
#                         --graph
#
# ./python/gen_results.py -i BRPR2012_sdk_2.json \
#                         --export-config config/export_config.json \
#                         -o .
#
# ./python/gen_results.py -i BRPR2012_sdk_2.json \
#                         --export-config config/export_config.json \
#                         --expected annotation/BRPR2012_ref.json \
#                         --compare_config config/compare_config.json \
#                         --highlight speak \
#                         --split
#                         -o report


import os
import sys
import copy
import math
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from utils import utils
from utils import filters
from utils import export_results
from utils import compare
from utils import graph

from collections import OrderedDict


def remove_initial_bias_emotions(emotions, fps, duration, start_time=0, add_to_neutrality=True):
    """ Remove initial bias on emotions, first seconds are expected to be neutral """

    start = fps * start_time
    samples = fps * (start_time + duration)

    # Initial bias
    bias = OrderedDict()
    for i in range(0, len(emotions)):
        bias[utils.EMOTIONS[i]] = np.mean(emotions[i][int(start):int(samples)])

    print("Bias")
    for i in range(0, len(emotions)):
        print(utils.EMOTIONS[i], bias[utils.EMOTIONS[i]])

    return remove_bias_values_emotions(emotions, bias)


def remove_bias_values_emotions(emotions, bias, add_to_neutrality=True):
    """ Remove given bias value for each emotions """

    emotions_b = copy.deepcopy(emotions)

    for i in range(0, len(emotions)):

        if i == utils.NEUTRALITY_INDEX:
            continue

        if utils.EMOTIONS[i] not in bias:
            continue

        for j, value in enumerate(emotions[i]):

            new_value = value - bias[utils.EMOTIONS[i]]
            if new_value < 0:
                new_value = 0
            sub = value - new_value

            emotions_b[i][j] = new_value

            # Add to neutrality
            if add_to_neutrality:
                emotions_b[utils.NEUTRALITY_INDEX][j] += sub

    return emotions_b


def apply_bias_filter_on_emotions(emotions, fps, missing, export_config):
    """ Apply bias filter on emotions """

    if not export_config:
        print("\nError: config file parameter is missing !!\n")
        return None

    # Remove bias
    emotions_b = filters.remove_bias(emotions, export_config, fps)

    return emotions_b


def apply_lowpass_filter_on_emotions(emotions, fps, missing, cfg):
    """ Apply lowpass filter on emotions """

    if not cfg:
        print("\nError: config file parameter is missing !!\n")
        return None

    emotions_f = np.zeros([len(emotions), len(emotions[0])])

    # Remove the peaks to have smooth results
    for i in range(0, len(emotions)):
        emotions_f[i] = filters.lowpass_filter(emotions[i], fps, tc=cfg[utils.EMOTIONS[i]])

    emotions_f[emotions_f < 0] = 0
    emotions_f[emotions_f > 1] = 1

    # Force to have gaps
    emotions_f[:, missing] = np.nan

    return emotions_f


def get_primary_secondary_emotions(emotions):
    """ Get primary and secondary emotions """

    frame_count = len(emotions[0])
    nb_emotions = len(emotions)

    emo_sort = np.argsort(emotions, axis=0)
    emo_1 = np.zeros([nb_emotions, frame_count])
    emo_2 = np.zeros([nb_emotions, frame_count])

    for i in range(0, frame_count):

        primary = emotions[emo_sort[nb_emotions-1, i]][i]
        emo_1[emo_sort[nb_emotions-1, i], i] = primary
        secondary = emotions[emo_sort[nb_emotions-2, i]][i]
        emo_2[emo_sort[nb_emotions-2, i], i] = secondary

    return emo_1, emo_2


def find_neutral_section(emotions, fps):
    """ Find a section where emotion is neutral and stable """

    start_time = 0
    duration = 10

    # TODO: find calm section when the person is not speaking

    return start_time, duration


def convert_diff_to_float(value):
    """ Convert a value / diff value to float """

    if isinstance(value, str):
        return float(value.split(' (')[0])
    else:
        return float(value)


def add_empty_lines(results):
    """ Add an empty line between sections """

    if len(results['data']) == 0:
        return results

    dict_data = {'data': []}

    empty_result = copy.deepcopy(results['data'][0])
    for key in empty_result.keys():
        empty_result[key] = ''

    t = convert_diff_to_float(results['data'][0]['time'])
    duration = convert_diff_to_float(results['data'][0]['duration'])

    last_end_time = t + duration

    min_distance_between_sections = 1  # Allowed gap between results in a section (in seconds)

    for result in results['data']:

        # Ignore empty lines
        if result['time'] == '':
            continue

        t = convert_diff_to_float(result['time'])
        duration = convert_diff_to_float(result['duration'])

        if t > last_end_time + min_distance_between_sections:
            dict_data['data'].append(copy.deepcopy(empty_result))

        dict_data['data'].append(copy.deepcopy(result))

        last_end_time = t + duration

    return dict_data


def add_diff_values(results, ignore_neutrality=True):
    """ Add the difference to expected result """

    dict_data = {'data': []}

    for result in results['data']:

        new_data = copy.deepcopy(result)

        add_diff = True

        if (ignore_neutrality) and (result['emotion'] == 'neutrality'):
            add_diff = False

        if 'diff' not in result:
            add_diff = False
        elif not isinstance(result['diff'], dict):
            add_diff = False

        if add_diff:

            new_data['time'] = "{0:.3f}".format(result['time']) + \
                                ' (' + "{0:.3f}".format(result['diff']['time']) + ')'
            new_data['duration'] = "{0:.3f}".format(result['duration']) + \
                                   ' (' + "{0:.3f}".format(result['diff']['duration']) + ')'

        dict_data['data'].append(new_data)

    return dict_data


def add_speak_information(results, expected):
    """ Add a tag to inform that the person was speaking """

    dict_data = {'data': []}

    for result in results['data']:

        is_speaking = False

        for exp in expected['data']:

            if 'speak' not in exp:
                continue

            if exp['speak'].lower() != 'yes':
                continue

            min_time = exp['time']
            res_end_time = result['time'] + result['duration']
            exp_end_time = exp['time'] + exp['duration']
            max_time = exp_end_time

            # Stop condition
            if exp['time'] > res_end_time:
                break

            if ((result['time'] >= min_time) and (result['time'] <= max_time)) or \
               ((res_end_time >= min_time) and (res_end_time <= max_time)) or \
               ((exp['time'] >= result['time']) and (exp_end_time <= res_end_time)):

                is_speaking = True
                break

        new_result = copy.deepcopy(result)
        new_result['speak'] = str(is_speaking)
        dict_data['data'].append(new_result)

    return dict_data


def split_results(results):
    """ Split the results per section """

    list_results = []

    if len(results['data']) == 0:
        return list_results

    section = {'data': []}

    t = convert_diff_to_float(results['data'][0]['time'])
    duration = convert_diff_to_float(results['data'][0]['duration'])

    last_end_time = t + duration

    min_distance_between_sections = 1  # Allowed gap between results in a section (in seconds)

    for result in results['data']:

        t = convert_diff_to_float(result['time'])
        duration = convert_diff_to_float(result['duration'])

        if t > last_end_time + min_distance_between_sections:

            list_results.append(section)
            section = {'data': []}

        section['data'].append(copy.deepcopy(result))

        last_end_time = t + duration

    list_results.append(section)

    return list_results


def dump_results(list_results, comments, folder,
                 input_name, video_name, nb_correct, nb_error, percentage_face_detected,
                 results_filename):
    """ Dump all results into a folder """

    os.makedirs(folder, exist_ok=True)

    output_data = OrderedDict()

    output_data['input'] = input_name
    output_data['video'] = video_name
    output_data['correct'] = nb_correct
    output_data['error'] = nb_error
    output_data['match_rate'] = 0
    if nb_correct + nb_error > 0:
        output_data['match_rate'] = (nb_correct / (nb_correct+nb_error)) * 100
    output_data['percentage_face_detected'] = percentage_face_detected

    output_data['data'] = []

    utils.write_json(list_results, os.path.join(folder, "results_split.json"))

    for i, section in enumerate(list_results):

        section['input'] = input_name

        if i < len(comments):
            section['comment'] = comments[i]

        filename = os.path.join(folder, "results" + str(i+1) + ".json")
        utils.write_json(section, filename)

        # Remove first folder from the path to be able to open it from html report
        sub_filename = os.path.join(*filename.split(os.sep)[1:])

        section_results = OrderedDict()
        section_results['results'] = sub_filename
        comment = ""
        if i < len(comments):
            comment = comments[i]

        section_results['comment'] = comment

        output_data['data'].append(section_results)

    results_filename = os.path.join(folder, results_filename)
    utils.write_json(output_data, results_filename)

    return


def main():
    """ Entry point """

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", action="store", required=True,
                        help="SDK output json file")
    parser.add_argument("-c", "--export-config", dest="export_config", action="store",
                        help="Export configuration file to use sliding window")

    parser.add_argument("--mode", action="store",
                        help="Export mode [window, dominant] or none for to show graphs",
                        default="window")

    # Comparison with expected results
    parser.add_argument("--expected", action="store", help="Expected json results")
    parser.add_argument("--compare-config", dest="compare_config", action="store",
                        help="Compare config file")
    parser.add_argument("--split", action="store_true",
                        help="Split the results into multiple files per section")
    parser.add_argument("--highlight", action="store", default='none',
                        help="Key of area to highlight (ex: speak)")
    parser.add_argument("--graph", action="store_true",
                        help="Show graphs")
    parser.add_argument("--export-graph", dest="export_graph", action="store",
                        help="Export the graphs to the image filename")
    parser.add_argument("--initial-bias-duration", dest="initial_bias_duration", type=int,
                        default=0, required=False,
                        help="Bias removal configuration. " +
                             "Initial bias duration or " +
                             "0 for auto detect and annotation configuration or " +
                             "-1 to apply an highpass filter")

    parser.add_argument("-o", "--output", action="store", help="Output folder")

    args = parser.parse_args()

    report = utils.open_json(args.input)
    export_config = utils.open_json(args.export_config)

    timestamps, emotions, missing = utils.parse_video_report(report)

    percentage_face_detected = 100 - (((len(missing)/7)/(len(emotions[0])))*100)
    print("Face detected:", percentage_face_detected, "%")

    dt = timestamps[1] - timestamps[0]
    fps = np.around(1 / dt)
    print("FPS:", fps)

    if args.expected:
        expected_results = utils.open_json(args.expected)

        if 'data' not in expected_results:
            print("Error: data list is mandatory in expected results, use empty.json annotation file format !")
            sys.exit(1)

    # Bias removal
    if args.initial_bias_duration < 0:

        # Remove bias using an highpass filter
        emotions_b = apply_bias_filter_on_emotions(emotions, fps, missing, export_config)

    elif (args.initial_bias_duration == 0) and (args.expected):

        # Remove bias using specific annotations
        if "bias_neutral" in expected_results:

            bias_neutral = expected_results["bias_neutral"]

            if "time" in bias_neutral:

                emotions_b = remove_initial_bias_emotions(emotions, fps,
                                                          bias_neutral["duration"],
                                                          bias_neutral["time"])
            elif "emotions" in bias_neutral:

                bias = bias_neutral["emotions"]
                emotions_b = remove_bias_values_emotions(emotions, bias)

            else:

                print("Error: invalid bias_neutral section in annotations !")
                sys.exit(1)

        else:

            # TODO: detect calm neutral section
            print("Error: calm neutral section detection not implemented yet !")
            sys.exit(1)

            start_time, duration = find_neutral_section(emotions, fps)

            emotions_b = remove_initial_bias_emotions(emotions, fps, duration, start_time)

    elif args.initial_bias_duration > 0:

        # Remove bias calculated during the first seconds
        emotions_b = remove_initial_bias_emotions(emotions, fps, args.initial_bias_duration)

    else:

        print("Error: no bias removal options !")
        sys.exit(2)

    emotions_f = apply_lowpass_filter_on_emotions(emotions_b, fps, missing,
                                                  export_config["window_size"]["independent"])

    emo_1, emo_2 = get_primary_secondary_emotions(emotions_b)

    results = None

    if args.mode == 'window':

        if args.output:
            print("Export results using sliding window")

        results = export_results.sliding_window(timestamps, emotions_b, export_config, "short")
        results_middle = export_results.sliding_window(timestamps, emotions_b, export_config, "middle")
        results_long = export_results.sliding_window(timestamps, emotions_b, export_config, "long")

    elif args.mode == 'dominant':

        if args.output:
            print("Export results using dominant emotion")

        results = export_results.get_emotion_areas_using_primary(timestamps,
                                                                 emo_1,
                                                                 utils.EMOTIONS)
    else:
        print("Error: export mode is invalid !")

    if results is None:
        sys.exit(1)

    if 'input' in report:
        video_name = os.path.basename(report['input'])
    else:
        video_name = "unknown video"

    if args.output:

        print(">> Export the results before comparison with expected results")

        results['input'] = args.input
        results['video'] = video_name

        utils.write_json(results, os.path.join(args.output, "results_sdk.json"), export_js=False)

        results_emotion = OrderedDict()
        results_emotion['data'] = []

        for k, t in enumerate(timestamps):
            info = OrderedDict()
            info['time'] = t

            for l, em in enumerate(utils.EMOTIONS):
                intensity = emotions_f[l][k]

                info['emotion'] = em
                info['intensity'] = intensity

                results_emotion['data'].append(copy.deepcopy(info))

        utils.write_json(results_emotion,
                         os.path.join(args.output, "results_emotion.json"), export_js=False)

    if (not args.expected) and args.compare_config:

        print("Error: --expected and --compare-config parameters are required for comparison !")
        sys.exit(1)

    if args.expected and args.compare_config:

        # Compare results

        compare_config = utils.open_json(args.compare_config)

        if args.mode == 'window':
            all_results = []
            all_results.append(results)
            if results_middle:
                all_results.append(results_middle)
            if results_long:
                all_results.append(results_long)
        else:
            all_results = results

        results_matches = compare.get_matches_results(all_results, expected_results, compare_config)
        output_data = results_matches

        # Add information to output results

        if args.highlight == 'speak':
            results_speak = add_speak_information(output_data, expected_results)
            output_data = results_speak

        results_diff = add_diff_values(output_data)
        output_data = results_diff

    if args.output:

        # Dump results to json and js files

        # TODO, key to use to compute match_rate, select the window size config to use
        match_key = 'match_short'

        if args.split:

            # Split results into multiple files

            list_results = split_results(output_data)

            comments = compare.get_section_comments(expected_results)

            nb_correct = 0
            nb_error = 0

            for result in results_matches['data']:

                if result[match_key] == 'True':
                    nb_correct += 1
                if result[match_key] == 'False':
                    nb_error += 1

            dump_results(list_results, comments, args.output,
                         args.input, video_name, nb_correct, nb_error, percentage_face_detected,
                         "results.json")

            utils.write_json(export_config, os.path.join(args.output, "config.json"))
            if args.compare_config:
                utils.write_json(compare_config, os.path.join(args.output, "compare_config.json"))

        # Export all results in one file

        results_sep = add_empty_lines(output_data)
        output_data = results_sep

        output_data['input'] = args.input
        output_data['video'] = video_name

        utils.write_json(output_data, os.path.join(args.output, "results_all.json"))

        # Export emotion averages for a given period of time
        short_term_averages = export_results.get_average_emotion(timestamps, emotions_b, 5)
        long_term_averages = export_results.get_average_emotion(timestamps, emotions_b)

        utils.write_json(short_term_averages, os.path.join(args.output, "short_term_averages.json"))
        utils.write_json(long_term_averages, os.path.join(args.output, "long_term_averages.json"))

        non_match_averages = export_results.get_non_match_averages(output_data, match_key)

        # Export emotion averages match rate
        utils.write_json(non_match_averages, os.path.join(args.output, "non_match_averages.json"))

        if not args.graph and not args.export_graph:
            sys.exit(0)

    #
    # Graphs
    #

    if not args.graph and not args.export_graph:
        sys.exit(0)

    # Normalize color to [0, 1]
    plot_colors = {}
    for key in utils.EMOTION_COLORS:
        rgb = utils.EMOTION_COLORS[key]
        plot_colors[key] = (rgb[0]/255, rgb[1]/255, rgb[2]/255)

    emocolors = []
    for i, estr in enumerate(utils.EMOTIONS):
        emocolors.append(plot_colors[estr])

    enable_primary_secondary_plot = False
    enable_histogram_plot = False
    enable_filters_plot = True
    if not args.export_config:
        enable_filters_plot = False

    # Graph of primary and secondary emotions
    if enable_primary_secondary_plot:

        for i, estr in enumerate(utils.EMOTIONS):
            if estr != "neutrality":
                plt.fill_between(timestamps, 0, emo_1[i], color=plot_colors[estr],
                                 label=estr, linewidth=0.1)
                plt.fill_between(timestamps, - emo_2[i], 0, color=plot_colors[estr],
                                 label=estr, linewidth=0.1)

    # Graph of emotion histogram
    if enable_histogram_plot:

        nb_emotions = len(utils.EMOTIONS)

        fig, axes = plt.subplots(nb_emotions, 1, sharex=True, sharey=True)

        axes[0].set_title("Emotion histograms")

        for i, val in enumerate(emotions):
            axes[i].hist(val, color=plot_colors[utils.EMOTIONS[i]])
            axes[i].set_xlabel(utils.EMOTIONS[i] + " confidence")

        axes[int(len(utils.EMOTIONS)/2)].set_ylabel("Frequency")

    # Graph of video emotions and filtered emotions
    if enable_filters_plot:

        subplot_count = 1
        # lowpass_index = 1
        bar_index = 1

        if args.export_config:
            subplot_count += 1
            # lowpass_index += 1
            bar_index += 1

        if args.mode == "window":
            subplot_count += 3

        fig, axes = plt.subplots(subplot_count, 1, sharex=True, sharey=True)

        axes[0].stackplot(timestamps, emotions, colors=emocolors, labels=utils.EMOTIONS)
        axes[0].set_title("Video processing emotion results")
        axes[0].set_ylim(-0.05, 1.05)

        if args.export_config:
            axes[1].stackplot(timestamps, emotions_b, colors=emocolors)
            axes[1].set_title("Results of bias removal")
            if args.mode != "window":
                axes[1].set_xlabel("Time (s)")

        # Graph of sliding window output
        if args.mode == "window":

            timestamps_bars = []
            timestamps_bars_middle = []
            timestamps_bars_long = []
            emotions_bars = OrderedDict()
            emotions_bars_middle = OrderedDict()
            emotions_bars_long = OrderedDict()

            for emo_string in utils.EMOTIONS:
                emotions_bars[emo_string] = []
                emotions_bars_middle[emo_string] = []
                emotions_bars_long[emo_string] = []

            timestamps_bars, emotions_bars = graph.transform_to_bar_plot_data(results,
                                                                              timestamps_bars,
                                                                              emotions_bars,
                                                                              'confidence')
            timestamps_bars_middle, emotions_bars_middle = graph.transform_to_bar_plot_data(results_middle,
                                                                                            timestamps_bars_middle,
                                                                                            emotions_bars_middle,
                                                                                            'confidence')
            timestamps_bars_long, emotions_bars_long = graph.transform_to_bar_plot_data(results_long,
                                                                                        timestamps_bars_long,
                                                                                        emotions_bars_long,
                                                                                        'confidence')

            for key, val in plot_colors.items():

                axes[bar_index].fill(timestamps_bars, emotions_bars[key], color=val, label=key,
                                     linewidth=0.1)
                axes[bar_index].set_title("Results of short sliding window")
                axes[bar_index].set_ylabel("Emotion confidence")

                axes[bar_index+1].fill(timestamps_bars_middle, emotions_bars_middle[key], color=val,
                                       label=key, linewidth=0.1)
                axes[bar_index+1].set_title("Results of middle sliding window")

                axes[bar_index+2].fill(timestamps_bars_long, emotions_bars_long[key], color=val,
                                       label=key, linewidth=0.1)
                axes[bar_index+2].set_title("Results of long sliding window")
                axes[bar_index+2].set_xlabel("Time (s)")

        axes[0].legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)

        if args.highlight != 'none':

            for res in expected_results['data']:

                if (args.highlight in res) and ((res[args.highlight] == "True") or (res[args.highlight] == "yes")):

                    start_x = res['time']
                    width = res['duration']

                    axes[0].add_patch(patches.Rectangle((start_x, -1.05), width, 2.1,
                                      fill=True, color='red', alpha=0.3, edgecolor='red'))
                    axes[1].add_patch(patches.Rectangle((start_x, -1.05), width, 2.1,
                                      fill=True, color='red', alpha=0.3, edgecolor='red'))

                    if subplot_count >= 3:
                        axes[2].add_patch(patches.Rectangle((start_x, -1.05), width, 2.1,
                                          fill=True, color='red', alpha=0.3, edgecolor='red'))

        if args.export_graph:

            fig.set_dpi(100)
            fig.set_size_inches(20, 12)
            dot_index = args.export_graph.rfind('.')
            export_graph_path = args.export_graph[:dot_index] + "_1." + args.export_graph[dot_index+1:]
            fig.savefig(export_graph_path)

        # Results independent per emotion
        fig, axes = plt.subplots(len(utils.EMOTIONS), 1, sharex=True)

        for i, emotion in enumerate(utils.EMOTIONS):

            timestamps_bars = []

            for emo_string in utils.EMOTIONS:
                emotions_bars[emo_string] = []

            axes[i].plot(timestamps, emotions_f[i], color=plot_colors[emotion],
                         label=key, linewidth=2)

            axes[i].set_title("Results of low pass filter (" +
                              str(export_config['window_size']['independent'][emotion]) + ") for " +
                              emotion,
                              fontsize=10)
            max_y = 1.1 * np.nanmax(emotions_f[i])
            if max_y < 0.1:
                max_y = 0.1
            axes[i].set_ylim(0, max_y)

        axes[int(len(utils.EMOTIONS)/2)].set_ylabel("Confidence")
        axes[len(utils.EMOTIONS)-1].set_xlabel("Time (s)")

        if args.export_graph:

            fig.set_dpi(100)
            fig.set_size_inches(20, 12)
            dot_index = args.export_graph.rfind('.')
            export_graph_path = args.export_graph[:dot_index] + "_2." + args.export_graph[dot_index+1:]
            fig.savefig(export_graph_path)

    if not args.export_graph:
        plt.show()


if __name__ == '__main__':
    main()
