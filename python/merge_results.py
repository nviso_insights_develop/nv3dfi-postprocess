#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Create a json file with match_rate of all results """

# Usage ex:
#
# ./python/merge_results.py -i output


import os
import sys
import argparse

from utils import utils

from collections import OrderedDict


def main():
    """ Entry point """

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", action="store", required=True,
                        help="Folder where all results has been generated")

    args = parser.parse_args()

    folders = []

    overall_match_rate = 0

    output_data = OrderedDict()
    output_data['data'] = []
    output_data['averages'] = OrderedDict()

    for folder in os.listdir(args.input):

        folder_path = os.path.join(args.input, folder)

        if os.path.isdir(folder_path):

            folders.append(folder)

    folders.sort()

    count_annotation = 0

    emotion_averages = OrderedDict()
    emotion_non_match_averages = OrderedDict()
    for emotion in utils.EMOTIONS:
        emotion_averages[emotion] = 0
        emotion_non_match_averages[emotion] = 0

    for folder in folders:

        folder_path = os.path.join(args.input, folder)
        results_file_path = os.path.join(folder_path, "results.json")

        if os.path.exists(results_file_path):

            results = utils.open_json(os.path.join(folder_path, "results.json"))
            match_rate = results['match_rate']
            overall_match_rate += match_rate
            data = OrderedDict()
            data['name'] = folder
            data['match_rate'] = match_rate
            output_data['data'].append(data)
            print(folder, " {0:.2f} %".format(match_rate))
            count_annotation += 1

            long_averages = utils.open_json(os.path.join(folder_path, "long_term_averages.json"))
            for emotion in utils.EMOTIONS:
                emotion_averages[emotion] += long_averages['percent'][0][emotion]

            non_match_averages = utils.open_json(os.path.join(folder_path, "non_match_averages.json"))
            for emotion in utils.EMOTIONS:
                emotion_non_match_averages[emotion] += non_match_averages['percent'][emotion]

        else:

            # Empty annotation
            data = OrderedDict()
            data['name'] = folder
            data['match_rate'] = -1
            output_data['data'].append(data)

        data['percentage_face_detected'] = results['percentage_face_detected']

    if len(output_data['data']) == 0:

        print("Error: no results available !")
        sys.exit(1)

    overall_match_rate /= count_annotation

    print("\nMatch rate  {0:.2f} %\n".format(overall_match_rate))

    output_data['match_rate'] = overall_match_rate

    for emotion in utils.EMOTIONS:
        emotion_averages[emotion] /= count_annotation
        emotion_non_match_averages[emotion] /= count_annotation

    output_data['averages']['long_term'] = emotion_averages
    output_data['averages']['non_match'] = emotion_non_match_averages

    utils.write_json(output_data, os.path.join(args.input, "results.json"))


if __name__ == '__main__':
    main()
