#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.

"""
    Split an output json file containing multiple face results
"""

import os
import copy
import argparse

from collections import OrderedDict

from utils import utils


def roi_contains(rect1, rect2):

    if (rect2['x'] > rect1['x']) and (rect2['y'] > rect1['y']):

        if (rect2['x'] + rect2['w'] < rect1['x'] + rect1['w']) and (rect2['y'] + rect2['h'] < rect1['y'] + rect1['h']):
            return True

    return False


def get_face_id(face, faces_roi):

    index = -1

    for i, roi in enumerate(faces_roi):

        if roi_contains(roi['roi'], face['detection']['location']):

            if (face['detection']['location']['w'] <= roi['max_size']):
                if (face['detection']['location']['h'] <= roi['max_size']):
                    if (face['detection']['location']['w'] >= roi['min_size']):
                        if (face['detection']['location']['h'] >= roi['min_size']):

                            # print(roi['roi'], face['detection']['location'])
                            index = roi['id']
                            break

    return index


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", action="store", required=True, help="Json input file")
    parser.add_argument("-c", "--options", action="store", required=True, help="Json options file")
    parser.add_argument("-n", "--number", action="store", required=True, help="Number of faces")
    parser.add_argument("-o", "--output", action="store", required=True, help="Output folder")

    args = parser.parse_args()

    if not os.path.isfile(args.input):
        print("Error: The file ", args.input, " does not exist !!")
        return False

    if not os.path.isfile(args.options):
        print("Error: The file ", args.options, " does not exist !!")
        return False

    os.makedirs(args.output, exist_ok=True)

    input_data = utils.open_json(args.input)
    options_data = utils.open_json(args.options)

    empty_frames = OrderedDict()
    empty_frames['frames'] = []

    faces = []
    for i in range(0, int(args.number)):
        faces.append(copy.deepcopy(empty_frames))

    for frame in input_data['frames']:

        if 'face_data' in frame:

            ids = []

            for face in frame['face_data']:

                if 'id' in face:

                    # TODO: replace with face.id when it's used correctly
                    face_id = get_face_id(face, options_data['input']['faces'])

                    if face_id < int(args.number) and face_id != -1:

                        face_data = OrderedDict()
                        face_data['face_data'] = [face]
                        face_data['video_pos_frame'] = frame['video_pos_frame']
                        face_data['video_pos_ms'] = frame['video_pos_ms']
                        faces[face_id]['frames'].append(face_data)

                        ids.append(face_id)

            # Add empty faces
            for i in range(0, int(args.number)):

                if i not in ids:

                    empty_face_data = OrderedDict()
                    empty_face_data['face_data'] = []
                    empty_face_data['video_pos_frame'] = frame['video_pos_frame']
                    empty_face_data['video_pos_ms'] = frame['video_pos_ms']

                    faces[i]['frames'].append(empty_face_data)

    # Write results
    for i in range(0, int(args.number)):
        utils.write_json(faces[i], os.path.join(args.output, 'face_' + str(i) + '.json'), export_js=False)


if __name__ == '__main__':
    main()
