#!/usr/bin/env python3
#
# NVISO CONFIDENTIAL
#
# Copyright (c) 2017 nViso SA. All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") is the confidential and proprietary information
# owned by nViso or its suppliers or licensors.  Title to the  Material remains
# with nViso SA or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of nViso or its
# suppliers and licensors. The Material is protected by worldwide copyright and trade
# secret laws and treaty provisions. You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the license
# agreement you entered into with nViso.
#
# NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
# THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
# ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
# DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
#


""" Extract image(s) or a gif from a video sequence """

# Usage ex:
#
# Images:
#
# ./python/get_video_image.py -i ~/Downloads/BRPR2012.avi -t 100 -s 1000
#
# Gif:
#
# ./python/get_video_image.py -i ~/Downloads/BRPR2012.avi -t 100 -m 1000 -o name.gif


import sys
import argparse
import imageio
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw, ImageFont


def main():
    """ Entry point """

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", action="store", required=True,
                        help="Input video file")
    parser.add_argument("-t", "--time", action="store", required=True,
                        help="Time in the video in seconds")
    parser.add_argument("-s", "--step", action="store", required=False, default='200',
                        help="In image mode, the time between images in ms")
    parser.add_argument("-m", "--margin", action="store", required=False, default='500',
                        help="In gif mode, the time (in ms) before and after the given time")
    parser.add_argument("-d", "--slow-down", action="store", required=False, default='1',
                        dest="slow_down", help="Gif slow down speed factor")
    parser.add_argument("-o", "--output", action="store", required=False,
                        help="Output image file, if not present show the image")

    args = parser.parse_args()

    filename = args.input
    vid = imageio.get_reader(filename, 'ffmpeg')

    vid_meta_data = vid.get_meta_data()

    fps = vid_meta_data['fps']
    dt = 1000/fps
    duration = vid_meta_data['duration']
    resolution = vid_meta_data['source_size']

    time = float(args.time)

    print("Video duration:", duration, "sec")
    print("Video fps:", fps)
    print("Video resolution:", resolution)

    print("Video at:", time, "sec")

    if time > duration:
        print("Error: invalid time (", time, ">", duration, ") !")
        sys.exit(1)

    frame_index = int(time * 1000 / dt)

    print(">> Time:", time, "sec.")
    print(">> Index:", frame_index, "/", vid.get_length())

    # Show frames before and after
    if frame_index == 0:
        nums = [frame_index, frame_index+1]
    else:
        ratio = int(int(args.step) / dt)
        nums = [frame_index-(1*ratio), frame_index, frame_index+(1*ratio)]

    # Export images
    if args.output:
        fig, axes = plt.subplots(1, len(nums))
        vertical_alignment = 'baseline'
    else:
        fig, axes = plt.subplots(1, len(nums), sharex=True, sharey=True)
        vertical_alignment = 'top'

    fig.tight_layout()

    for i, num in enumerate(nums):
        image = vid.get_data(num)
        axes[i].set_title("{0:.2f} sec.".format(num*dt/1000), verticalalignment=vertical_alignment)
        axes[i].axis("off")

        if (args.output) and ('.gif' not in args.output):
            imageio.imwrite(args.output.replace('.', '_' + str(num) + '_.'), image)

        axes[i].imshow(image)

    if args.output:
        if '.gif' in args.output:

            ratio = int(int(args.margin) / dt)
            min_index = frame_index-(1*ratio)
            max_index = frame_index+(1*ratio)
            if min_index < 0:
                min_index = 0
            if max_index > vid.get_length():
                max_index = vid.get_length()

            nb_images = max_index - min_index
            point_size = 50
            height = resolution[1] - (2*point_size)
            sep = (resolution[0] - (4*50)) / nb_images
            slow_down = float(args.slow_down)

            print("Export GIF [", (min_index * dt / 1000), ",", (max_index * dt / 1000), "]")

            display_time = "{0:.2f} sec.".format(min_index * dt / 1000)
            display_speed = "x{0:.2f}".format(1/slow_down)

            mod = 1
            if fps > 20:
                mod = 2

            images = []
            for i in range(min_index, max_index):

                if i % mod == 0:
                    continue

                # Draw on image
                image = vid.get_data(i)
                pil_image = Image.fromarray(image)
                draw = ImageDraw.Draw(pil_image)
                point_position = (i-min_index+2) * (sep)
                if i % 3 == 0:
                    display_time = "{0:.2f} sec.".format(i * dt / 1000)
                draw.text((10, 10),
                          display_time,
                          font=ImageFont.truetype("FreeMono.ttf", size=42))
                draw.text((resolution[0]-(resolution[0]/8), 10),
                          display_speed,
                          font=ImageFont.truetype("FreeMono.ttf", size=42))
                draw.ellipse([(point_position, height),
                              (point_position + point_size, height + point_size)],
                             fill='white')

                images.append(np.array(pil_image))

            # Save images to a gif file
            imageio.mimsave(args.output, images, format='GIF', duration=slow_down/(fps/mod))

        else:

            print("Export images")
            fig.savefig(args.output)

    else:
        plt.show()


if __name__ == '__main__':
    main()
