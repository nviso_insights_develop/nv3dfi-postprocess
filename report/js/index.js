// NVISO CONFIDENTIAL
//
// Copyright (c) 2017 nViso SA. All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") is the confidential and proprietary information
// owned by nViso or its suppliers or licensors.  Title to the  Material remains
// with nViso SA or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of nViso or its
// suppliers and licensors. The Material is protected by worldwide copyright and trade
// secret laws and treaty provisions. You shall not disclose such Confidential
// Information and shall use it only in accordance with the terms of the license
// agreement you entered into with nViso.
//
// NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
// ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
// DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.


"use strict";

(function() {

    var charts = [];
    var tableNames = [];
    var comments = [];

    function drawAllGraphs() {

        drawBarGraph(json_long_term_averages["percent"][0], emotionColors,
                     "average_chart_1", "Long term emotions statistics", "Percentage", charts);
        drawGraphs(json_results_all, "emotion", emotions, emotionColors,
                   "emotion_chart_1", "emotion_chart_2", charts);
        drawGraphs(json_results_all, "intensity", limits, limitColors,
                   "intensity_chart_1", "intensity_chart_2", charts);
        drawGraphs(json_results_all, "certainty", limits, limitColors,
                   "certainty_chart_1", "certainty_chart_2", charts);
    }

    // Set report title
    var title = document.getElementById("title");
    title.innerHTML += ' ' + json_results['input'].split('/').reverse()[0].replace('.json', '');

    var videoName = document.getElementById("video_name");
    videoName.innerHTML += ' ' + json_results['video'];

    var countResults = document.getElementById("count_results");
    var nbErrors = json_results['error'];
    var nbCorrect = json_results['correct'];
    var nbResults = nbCorrect + nbErrors;
    countResults.innerHTML += 'The following results are using the short term window size configuration.';
    countResults.innerHTML += '</br>';
    countResults.innerHTML += '</br>';
    countResults.innerHTML += 'Number of correct results: ' + nbCorrect + " / " + nbResults;
    countResults.innerHTML += '</br>';
    countResults.innerHTML += 'Match rate: ' + Number(json_results['match_rate']).toFixed(2) + " %";
    countResults.innerHTML += '</br>';
    countResults.innerHTML += 'Percentage of faces detected: ' + Number(json_results['percentage_face_detected']).toFixed(2) + " %";

    // Graphs
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawAllGraphs);

    document.getElementById("compare_config_json").innerHTML = JSON.stringify(json_compare_config, null, 4);

    // Results per section
    for (var i in json_results['data']) {

        var comment = json_results['data'][i]['comment'];
        comments.push(comment);

        var tableName = "table" + i;
        tableNames.push(tableName);

        addHtmlTitle("container", comment);

        createTable(tableName, json_results_split[i]);
    }

    document.getElementById("config_json").innerHTML = JSON.stringify(json_config, null, 4);

    // Export to PDF
    var pdfButton = document.getElementById('pdf_button');
    pdfButton.addEventListener('click', function () {

        var pdf = new jsPDF();

        var title = document.getElementById("title").innerHTML;

        pdf.text(title, 20, 20);

        // Graphs
        var offsetH = 40;
        var ratio = 1.8

        var ret = addGraphsToPdf(pdf, charts[0]['chart'], "average_chart_1", 0, offsetH, ratio);
        var height = ret['height'];

        ret = addGraphsToPdf(pdf, charts[1]['chart1'], "emotion_chart_1", 0, 1.5*height + offsetH, ratio);
        var width = ret['width'];
        addGraphsToPdf(pdf, charts[1]['chart2'], "emotion_chart_2", width - (width * 0.2), 1.5*height + offsetH, ratio);

        addGraphsToPdf(pdf, charts[2]['chart1'], "intensity_chart_1", 0, 3*height + offsetH, ratio);
        addGraphsToPdf(pdf, charts[2]['chart2'], "intensity_chart_2", width - (width * 0.2), 3*height + offsetH, ratio);

        addGraphsToPdf(pdf, charts[3]['chart1'], "certainty_chart_1", 0, 4.5*height + offsetH, ratio);
        addGraphsToPdf(pdf, charts[3]['chart2'], "certainty_chart_2", width - (width * 0.2), 4.5*height + offsetH, ratio);

        // Tables

        // TODO: this ratio is for a zoom of 100%
        var ratio = 10;

        var pdf_lock = false;
        var i = 0;

        // TODO: use css to define print export format instead of html2canvas and jspdf
        function unlock_pdf() {

            pdf_lock = false;
            i += 1;
        }
        function next_table() {

            if (pdf_lock) {
                setTimeout(next_table, 200);
                return;
            }

            if (i >= tableNames.length) {
                pdf.save("comparison_report.pdf");
                return
            }

            pdf_lock = true;

            pdf.addPage();
            pdf.text(comments[i], 20, 30);
            addTableToPdf(pdf, tableNames[i], 10, 60, ratio, unlock_pdf);

            setTimeout(next_table, 200);
        }

        next_table();

    }, false);
})();
