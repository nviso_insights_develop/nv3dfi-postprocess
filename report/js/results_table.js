// NVISO CONFIDENTIAL
//
// Copyright (c) 2017 nViso SA. All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") is the confidential and proprietary information
// owned by nViso or its suppliers or licensors.  Title to the  Material remains
// with nViso SA or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of nViso or its
// suppliers and licensors. The Material is protected by worldwide copyright and trade
// secret laws and treaty provisions. You shall not disclose such Confidential
// Information and shall use it only in accordance with the terms of the license
// agreement you entered into with nViso.
//
// NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
// ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
// DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.


"use strict";

(function($){

    createTable("table1", json_results_all);

    // Export to PDF
    var pdfButton = document.getElementById('pdf_button');
    pdfButton.addEventListener('click', function () {

        var pdfButton = document.getElementById('pdf_button');
        pdfButton.style.display = "none";

        var pdf = new jsPDF();

        pdf.text('Comparison with expected results', 20, 20)

        // TODO: this ratio is for a zoom of 100%
        var ratio = 10;

        addTableToPdf(pdf, "container", 10, 40, ratio, function() {
            pdf.save("comparison_results.pdf");
        });

        pdfButton.style.display = "block";

    }, false);

} (jQuery));
