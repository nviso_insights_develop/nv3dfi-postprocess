// NVISO CONFIDENTIAL
//
// Copyright (c) 2017 nViso SA. All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") is the confidential and proprietary information
// owned by nViso or its suppliers or licensors.  Title to the  Material remains
// with nViso SA or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of nViso or its
// suppliers and licensors. The Material is protected by worldwide copyright and trade
// secret laws and treaty provisions. You shall not disclose such Confidential
// Information and shall use it only in accordance with the terms of the license
// agreement you entered into with nViso.
//
// NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
// ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
// DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.


"use strict";


var emotions = ['happiness', 'surprise', 'sadness', 'disgust', 'fear', 'anger', 'neutrality'];

var emotionColors = {
    'neutrality': '#b2b0a1',
    'anger': '#262626',
    'fear': '#86bc29',
    'disgust': '#824c98',
    'sadness': '#4dbdc8',
    'surprise': '#fdc41d',
    'happiness': '#d90c3e'
};

var limits = [0.2, 0.4, 0.6, 0.8, 1];

// Blue
//var limit_colors = ['#66ccff', '#3399ff', '#0066ff', '#0000cc' ,'#000066'];
// Green
var limitColors = ['#66ff33', '#00cc00', '#009933', '#006600' ,'#003300'];


function readJsonFile(filename, callback) {

    // Read a json file

    var xmlHttp = new XMLHttpRequest();

    xmlHttp.overrideMimeType("application/json");
    xmlHttp.open("GET", filename, true);
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.onreadystatechange = function() {

        if (xmlHttp.readyState === 4 && xmlHttp.status == "200") {

            callback(JSON.parse(xmlHttp.responseText));
        }
    }

    xmlHttp.send(null);
}


function addHtmlTitle(containerId, title) {

    // Add a title to the given container

    var container = document.getElementById(containerId);
    container.innerHTML += "<br/>"
    container.innerHTML += "<h3>" + title + "</h3>"
}

