// NVISO CONFIDENTIAL
//
// Copyright (c) 2017 nViso SA. All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") is the confidential and proprietary information
// owned by nViso or its suppliers or licensors.  Title to the  Material remains
// with nViso SA or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of nViso or its
// suppliers and licensors. The Material is protected by worldwide copyright and trade
// secret laws and treaty provisions. You shall not disclose such Confidential
// Information and shall use it only in accordance with the terms of the license
// agreement you entered into with nViso.
//
// NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
// ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
// DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.


"use strict";

google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(main);

var charts = [];


function main() {

    drawBarGraph(json_long_term_averages["percent"][0], emotionColors,
                 "average_chart_1", "Long term emotions statistics", "Percentage", charts);
    drawGraphs(json_results_all, "emotion", emotions, emotionColors,
               "emotion_chart_1", "emotion_chart_2", charts);
    drawGraphs(json_results_all, "intensity", limits, limitColors,
               "intensity_chart_1", "intensity_chart_2", charts);
    drawGraphs(json_results_all, "certainty", limits, limitColors,
               "certainty_chart_1", "certainty_chart_2", charts);
}


// Export to PDF
var pdfButton = document.getElementById('pdf_button');
pdfButton.addEventListener('click', function () {

    var pdf = new jsPDF();

    //pdf.text('Graphs', 20, 20);

    var offsetH = 20;

    var ratio = 1.8

    var ret = addGraphsToPdf(pdf, charts[0]['chart'], "average_chart_1", 0, offsetH, ratio);
    var height = ret['height'];

    ret = addGraphsToPdf(pdf, charts[1]['chart1'], "emotion_chart_1", 0, 2*height + offsetH, ratio);
    var width = ret['width'];
    addGraphsToPdf(pdf, charts[1]['chart2'], "emotion_chart_2", width - (width * 0.2), 2*height + offsetH, ratio);

    addGraphsToPdf(pdf, charts[2]['chart1'], "intensity_chart_1", 0, 4*height + offsetH, ratio);
    addGraphsToPdf(pdf, charts[2]['chart2'], "intensity_chart_2", width - (width * 0.2), 4*height + offsetH, ratio);

    addGraphsToPdf(pdf, charts[3]['chart1'], "certainty_chart_1", 0, 6*height + offsetH, ratio);
    addGraphsToPdf(pdf, charts[3]['chart2'], "certainty_chart_2", width - (width * 0.2), 6*height + offsetH, ratio);

    pdf.save("graphs.pdf");
}, false);
