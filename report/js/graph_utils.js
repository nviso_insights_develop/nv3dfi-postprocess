// NVISO CONFIDENTIAL
//
// Copyright (c) 2017 nViso SA. All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") is the confidential and proprietary information
// owned by nViso or its suppliers or licensors.  Title to the  Material remains
// with nViso SA or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of nViso or its
// suppliers and licensors. The Material is protected by worldwide copyright and trade
// secret laws and treaty provisions. You shall not disclose such Confidential
// Information and shall use it only in accordance with the terms of the license
// agreement you entered into with nViso.
//
// NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
// ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
// DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.


"use strict";


var graph_options = {
    title: '',
    //chartArea: {width: '70%', height: '70%'},
    legend: { position: "none" },
    tooltip: { trigger: "selection" },
    sliceVisibilityThreshold: 0,
    hAxis: {
        textStyle: {
            fontSize: 12,
            color: '#4d4d4d'
        },
        titleTextStyle: {
            fontSize: 18,
            color: '#4d4d4d'
        }
    },
    vAxis: {
        minValue: 0,
        title: '',
        format: 'percent',
        //maxValue: 1, // Up to 100%
        textStyle: {
            fontSize: 14,
            color: '#848484'
        },
        titleTextStyle: {
            fontSize: 14,
            color: '#848484'
        }
    }
};


function drawBarGraph(jsonData, colors, chartId, title, yTitle,
                      charts) {

    var values = [];
    values.push(['Element', 'Value', {role: 'style'}]); // Header

    for (var key in jsonData) {

        values.push([key.toString(),
                    jsonData[key],
                    colors[key]]);
    }

    var dataGoogle = google.visualization.arrayToDataTable(values);
    var chart = new google.visualization.ColumnChart(document.getElementById(chartId));
    graph_options.title = title;
    graph_options.vAxis.title = yTitle;
    graph_options.legend.position = "none";
    chart.draw(dataGoogle, graph_options);

    charts.push({'chart': chart});
}


function drawPieGraph(jsonData, typeColors, chartId, title, yTitle,
                      charts) {

    var colors = [];
    for (var key in typeColors) {
        colors.push(typeColors[key]);
    }

    var values = [];
    values.push(['Element', 'Value']); // Header

    for (var key in jsonData) {

        values.push([key.toString(), jsonData[key]]);
    }

    var dataGoogle = google.visualization.arrayToDataTable(values);
    var chart = new google.visualization.PieChart(document.getElementById(chartId));
    graph_options.title = title;
    graph_options.vAxis.title = yTitle;
    graph_options.legend.position = 'right';
    graph_options.legend.alignment = 'end';
    graph_options.colors = colors;
    graph_options.is3D = true;

    chart.draw(dataGoogle, graph_options);

    charts.push({'chart': chart});
}


function drawGraphs(jsonData, type, listKeys, typeColors, chart1Id, chart2Id,
                    charts) {

    // Draw 2 graphs, one pie chart and one bar chart

    // TODO: use drawPieGraph and drawBarGraph

    var colors = [];
    for (var key in typeColors) {
        colors.push(typeColors[key]);
    }

    var countKey = {};
    var countKeyErrors = {};

    for (var i=0; i<listKeys.length; i++) {

        countKey[listKeys[i]] = 0;
        countKeyErrors[listKeys[i]] = 0;
    }

    var total = 0;

    $.each(jsonData['data'], function(key, value){

        if ( (type in value) && (value[type] != '') ) {

            if ( listKeys.indexOf(value[type]) > -1 ) {

                countKey[value[type]] += 1;

                var matchKey = 'match_short';

                if ( (matchKey in value) && (value[matchKey] != '') ) {

                    if (value[matchKey] == 'False')
                        countKeyErrors[value[type]] += 1
                }
            }
        }
    });

    // Distribution
    var keyDistribution = [];
    keyDistribution.push(['Element', 'Value']); // Header

    for (var i=0; i<listKeys.length; i++) {
        keyDistribution.push([listKeys[i].toString(), countKey[listKeys[i]]]);
    }

    // Errors
    var keyErrors = [];
    keyErrors.push(['Element', 'Value', {role: 'style'}]); // Header

    for (var i=0; i<listKeys.length; i++) {
        keyErrors.push([listKeys[i].toString(),
                        countKeyErrors[listKeys[i]] / countKey[listKeys[i]],
                        colors[i]]);
    }

    // Graph settings

    // Distribution
    var dataGoogle = google.visualization.arrayToDataTable(keyDistribution);
    var chartKeyDistribution = new google.visualization.PieChart(document.getElementById(chart1Id));

    type = type.charAt(0).toUpperCase() + type.slice(1);

    graph_options.title = type + ' annotation distribution';
    graph_options.legend.position = 'right';
    graph_options.legend.alignment = 'end';
    graph_options.colors = colors;
    graph_options.is3D = true;
    chartKeyDistribution.draw(dataGoogle, graph_options);

    // Errors
    var dataGoogle = google.visualization.arrayToDataTable(keyErrors);
    var chartkeyErrors = new google.visualization.ColumnChart(document.getElementById(chart2Id));
    graph_options.title = 'Non-matching annotation per ' + type.toLowerCase();
    graph_options.vAxis.title = 'Percentage';
    graph_options.legend.position = 'none';
    chartkeyErrors.draw(dataGoogle, graph_options);

    charts.push({'chart1': chartKeyDistribution, 'chart2': chartkeyErrors});
}


function addGraphsToPdf(pdf, chart, chartId, x, y, scaleRatio) {

    // Add graphs to a pdf

    var divChart = document.getElementById(chartId);
    var divWidth = divChart.offsetWidth;
    var divHeight = divChart.offsetHeight;
    var ratio = divHeight / divWidth;

    var width = pdf.internal.pageSize.width / scaleRatio;
    var height = width * ratio;

    pdf.addImage(chart.getImageURI(), x, y, width, height);

    return {'width': width, 'height': height};
}
