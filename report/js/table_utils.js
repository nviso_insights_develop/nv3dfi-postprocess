// NVISO CONFIDENTIAL
//
// Copyright (c) 2017 nViso SA. All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") is the confidential and proprietary information
// owned by nViso or its suppliers or licensors.  Title to the  Material remains
// with nViso SA or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of nViso or its
// suppliers and licensors. The Material is protected by worldwide copyright and trade
// secret laws and treaty provisions. You shall not disclose such Confidential
// Information and shall use it only in accordance with the terms of the license
// agreement you entered into with nViso.
//
// NVISO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NVISO SHALL NOT BE LIABLE FOR
// ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
// DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.


"use strict";


function addHtmlTable(containerId, name) {

    // Add an html table to the given container

    var container = document.getElementById(containerId);
    container.innerHTML += "<br/>"
    container.innerHTML += "<table id=\"" + name + "\" class=\"display cell-border\" width=\"100%\" cellspacing=\"0\"><thead><tr><th></th></tr></thead></table>";
    container.innerHTML += "<br/>"
}


function createTable(tableName, jsonData) {

    // Create a DataTable

    addHtmlTable("container", tableName);

    // Read the json to extract the header, column names
    var columnsDatatable = [];
    var Columnsth = [];
    $.each(jsonData['data'][0], function(key, val){
        if ( (key != 'speak') && (key != 'diff') && (key != 'occlusion') ) {
            columnsDatatable.push({"data": key});
            Columnsth.push(key);
        }
    });

    // Add the data from the json file to the datatable

    // Formatting the table header
    var header = document.getElementById(tableName).getElementsByTagName('th');
    header[0].innerHTML = Columnsth[0];

    for(var i=1;i<Columnsth.length;i++){
        var new_header = document.createElement('th');
        new_header.innerHTML = Columnsth[i];
        header[i-1].parentNode.insertBefore(new_header, header[i-1].nextSibling);
    }

    // Datatable
    $('#' + tableName).DataTable(
        {
            data: jsonData['data'],

            rowCallback: function(row, data, index)
            {
                var nbMatchCols = 3;

                for(var i=0; i < columnsDatatable.length; i++)
                {
                    if (i < columnsDatatable.length - nbMatchCols)
                    {
                        if (data['emotion'] == 'neutrality') {
                            $('td',row).eq(i).css('background-color', '#b2b0a1');
                        };
                        if (data['emotion'] == 'anger') {
                            $('td',row).eq(i).css('background-color', '#262626');
                        };
                        if (data['emotion'] == 'fear') {
                            $('td',row).eq(i).css('background-color', '#86bc29');
                        };
                        if (data['emotion'] == 'disgust') {
                            $('td',row).eq(i).css('background-color', '#824c98');
                        };
                        if (data['emotion'] == 'sadness') {
                            $('td',row).eq(i).css('background-color', '#4dbdc8');
                        };
                        if (data['emotion'] == 'surprise') {
                            $('td',row).eq(i).css('background-color', '#fdc41d');
                        };
                        if (data['emotion'] == 'happiness') {
                            $('td',row).eq(i).css('background-color', '#d90c3e');
                        };
                    }
                    else
                    {
                        var types = ["short", "middle", "long"];

                        var matchKey = 'match_' + types[(i - (columnsDatatable.length-nbMatchCols))];

                        if (data[matchKey] == 'True') {
                            $('td',row).eq(i).css('background-color', '#39e600'); // Green
                        }
                        if (data[matchKey] == 'False') {
                            $('td',row).eq(i).css('background-color', '#ff3333'); // Red
                        }
                        if (data[matchKey] == '') {
                            if (data['emotion'] == 'neutrality') {
                                $('td',row).eq(i).css('background-color', '#a1a1a1'); // Grey
                            }
                            else {
                                $('td',row).eq(i).css('background-color', '#ffffff'); // White
                            }
                        }

                        var info = '';
                        if ( ('speak' in data) && (data['speak'] == 'True') ) {
                            info += ' (speak)';
                        }
                        if ( ('occlusion' in data) && (data['occlusion'] != 'none') ) {
                            info += ' (occlusion)';
                        }
                        $('td',row).eq(i).html(data[matchKey] + info);
                    }

                };
            },

            columns: columnsDatatable,

            paging: false,
            bSort: false,
            bInfo: false,
            searching: false,
            dom: 'Bfrtip'
        }
    );
}


function addTableToPdf(pdf, container, x, y, ratio, callback) {

    // Add a table to a pdf

    html2canvas(document.querySelector("#" + container)).then(canvas => {

        console.log("ON RENDERED " + container);

        var imgTable = canvas.toDataURL("image/png");
        var width = canvas.width / ratio;
        var height = canvas.clientHeight / ratio;

        pdf.addImage(imgTable, 'PNG', x, y, width, height);

        callback();
    });
}
